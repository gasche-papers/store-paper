#!/bin/bash
MAIN=store
EXTRATEX="fig-cnodes.tex fig-spec.tex macrobenchmark-tables.tex macros.tex"
BIB=store

mkdir arxiv || { echo "error, you need to (rmdir arxiv)"; exit 1; }
rm -f arxiv.zip

cp $MAIN.tex $MAIN.cfg arxiv/

cp $BIB.bib arxiv/ # (unused) source .bib, for reference
stat $BIB.bbl > /dev/null || { echo "you need to run bibtex first"; exit 1; }
cp $BIB.bbl arxiv/

for f in $EXTRATEX
do
    cp $f arxiv/
done

cp *.sty arxiv/
cp *.pgf arxiv/

cp acmart.cls arxiv/
cp acmauthoryear.{bbx,cbx} arxiv/

(
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
) > arxiv/build.sh

zip -r arxiv arxiv
echo "feel free to test the packed source in arxiv/, archive is arxiv.zip"
