literal:Basile Clément <basile.clement@ocamlpro.com>==>Anonymous Green <green@anon>
literal:Basile Clément==>Anonymous Green
literal:Basile==>Green
literal:basile.clement==>green

literal:Gabriel Scherer <gabriel.scherer@gmail.com>==>Anonymous Red <red@anon>
literal:Gabriel Scherer <gabriel.scherer@inria.fr>==>Anonymous Red <red@anon>
literal:Gabriel Scherer==>Anonymous Red
literal:Gabriel==>Red
literal:gasche==>red

literal:Clément==>Black
literal:Clement==>Black

literal:Guillaume Bury <guillaume.bury@ocamlpro.com>==>Anonymous Pink <pink@anon>
literal:Guillaume Bury <guillaume.bury@gmail.com>==>Anonymous Pink <pink@anon>
literal:Guillaume Bury==>Anonymous Pink
literal:Guillaume==>Pink

literal:François Pottier <francois.pottier@inria.fr>==>Anonymous Gold <gold@anon>
literal:François Pottier==>Anonymous Gold
literal:François==>Gold

literal:INRIA Saclay==>Somewhere
literal:INRIA Paris==>Somewhere
literal:Inria Saclay==>Somewhere
literal:Inria Paris==>Somewhere
literal:INRIA==>Somewhere
literal:Inria==>Somewhere

literal:https://gitlab.inria.fr/fpottier/unionfind==>https://some.where

literal:bench/StoreBasile.ml==>bench/StoreGreen.ml
literal:bench/StoreBasileImpl.ml==>bench/StoreGreenImpl.ml
literal:bench/StoreClement.ml==>bench/StoreBlack.ml
literal:bench/StoreClementImpl.ml==>bench/StoreBlackImpl.ml
literal:bench/StoreClementLeak.ml==>bench/StoreBlackLeak.ml
literal:bench/StoreClementNoleak.ml==>bench/StoreBlackNoleak.ml
literal:bench/StoreClementImplLeak.ml==>bench/StoreBlackImplLeak.ml
literal:bench/StoreClementImplNoleak.ml==>bench/StoreBlackImplNoleak.ml
