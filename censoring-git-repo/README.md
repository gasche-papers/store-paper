# Producing an anonymized git repository for 'store'

```
# from this subdirectory, run:

bash clone-command.sh
# this will create a fresh /tmp/store.git repository
# (destroys the previous content of /tmp/store.git)

bash censor.sh
# this will modify /tmp/store.git to anonimize it

# you can now test the content
cd /tmp/store.git
git log
grep -i -E "(Basile|Gabriel|Cl[ée]ment|Guillaume|Fran[cç]ois)" -R .
```

For more details, see https://gitlab.com/gasche-snippets/git-anonimization-tips
