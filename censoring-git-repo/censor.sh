if [ ! -d /tmp/store.git ]
then
    echo "Error: /tmp/store.git is missing. Run clone.sh first".
    exit 2
fi

if [ ! -f censor.sh ]
then
    echo "Error: this script must run from the 'censoring-git-repo' directory."
    exit 3
fi

DIR=$(pwd)

set -x
cd /tmp/store.git

git remote remove origin
rm .git/FETCH_HEAD

git branch -D marvin
git branch -D implementation-improvements
git gc --aggressive --force

git filter-repo \
  --mailmap $DIR/mailmap-anon \
  --replace-text $DIR/to-censor.txt \
  --replace-message $DIR/to-censor.txt \
  --paths-from-file $DIR/to-censor.txt \
  --refname-callback '
    refname = re.sub(b"clement",b"black",refname)
    refname = re.sub(b"jfla",b"custom",refname)
    return refname
    ' \
  --force

rm -fR .git/filter-repo
