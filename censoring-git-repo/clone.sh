set -x
rm -fR /tmp/store.git
git clone https://gitlab.com/basile.clement/store.git /tmp/store.git

cd /tmp/store.git
# mumbo-jumbo from https://stackoverflow.com/questions/10312521/how-do-i-fetch-all-git-branches
git branch -r | grep -v '\->' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
git fetch --all
git pull --all
