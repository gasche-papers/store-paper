ICFP 2024 Paper #48 Reviews and Comments
===========================================================================
Paper #48 Snapshottable stores


Review #48A
===========================================================================

Overall merit
-------------
B. OK paper, but I will not champion it

Reviewer Expertise
------------------
Y. I am knowledgeable in this area, but not an expert

Reviewer Confidence
-------------------
3. Good: I am reasonably sure of my assessment

Paper summary
-------------
This paper introduces a library Store moving efficiently between
multiple states of a mutable store. It provides more than just
backtracking, as one can retain any state and go back it after
arbitrary state switches (i.e., one is not restricted to linearly
ordered states).
This library is imperative, efficient, and its core algorithm was
proved using Iris.
Both micro- and macro-benchmark demonstrate its efficiency.

Assessment of the paper
-----------------------
Strengths:
* Having a proved library for a non-trivial algorithm is a boon. The
  benchmarks show that the library is highly efficient.
* Explanations about the snapshot tree are very clear.

Weakness:
* The benchmarks essentially only compare to other library-based
  approaches, where Store uniformly matches the best.
  The only comparison to a hand optimized approach the Sudoku-solver,
  for which the hand optimized approach is 17% faster than Store.
* Explanations about transactions and their interactions with snapshot
  are much less clear.

Conclusion:
This is a nice library with many fine applications.
However, I am not sure this is the ultimate solution.
Namely, people will probably still work out their own solution to grab
the few extra percents of performance.

Comments for authors
--------------------
Overall this paper is an enjoyable read.
This looks like a nice and versatile library working with multiple
states.

My only concern is that the comparison focuses on generic libraries.
As you note yourself, there are only relatively few of them, and most
people seem to roll out their own implementation for a specific use
case. The reason is that the use cases are specialized enough and
resource bound enough that one wants the best performance. So it is
more convincing to compare with such specialized implementation by
using Store in place of it, as you have done for Sudoku-solver.
I am afraid that in many cases there will indeed be some advantage,
either in performance or memory usage to use a hand-optimized
implementation.

This said, at the very least this looks like a great tool for
prototyping, and amply sufficient if you don't need the last 20% or
so.

Other comments:
* p.8 l.362: What is the meaning of "Mem"
* p.11 l.512:
  The benefit of record elision seems to depend much on the use case.
  For instance, in union-find, one updates a reference cell at most
  once, so I would expect to see no benefit.
* p.14 l.671: "Notice that this is not a map".
  What is not a map ? I did not understand.
* l.678: What is a camera ?
* p.15 sec.4.2: I am afraid I do not understand how transactions
  interact with snapshots. In particular I am confused about the
  invalidation part. Does it mean that invalidating a transaction does
  not invalidate transactions that were started after it?
* sec.4.3: I am still lost on the interaction between transaction and
  snapshot.



Review #48B
===========================================================================

Overall merit
-------------
B. OK paper, but I will not champion it

Reviewer Expertise
------------------
Y. I am knowledgeable in this area, but not an expert

Reviewer Confidence
-------------------
3. Good: I am reasonably sure of my assessment

Paper summary
-------------
This paper presents a "snapshottable store": an implementation of ML-style references (allocate, load, store) with the option to make a snapshot of a bag of references and roll back to prior versions. While there exists specialized data structures with snapshots, the one introduced in the paper is claimed to be sufficiently general that it can be used to implement other snapshottable data structures.

The contributions of the paper are:

+ A description of the implementation: a simple "core" version (based on Baker's version trees), an optimized version (with a new technique called "record elision"), and an extended version with transactions.
+ A mechanized proof of correctness of the "core" version in the Iris framework in Coq.
+ An implementation of the extended version in OCaml, which has been tested on micro and macro benchmarks.

Assessment of the paper
-----------------------
I found the paper interesting and pleasant to read. The paper clearly motivates the problem, the data structure, the way it is used, and the reasons why it is correct.

I have some smaller concerns:

+ The "core" version is verified, while only a proof sketch for optimized version with record elision is claimed to exist. Since the "core" version is essentially Baker's (the authors state that on p10) and record elision is the key optimization introduced in this paper, I would expect the full proof to be present.
+ The macro benchmarks appear limited. It seems that a version of Inferno with "store" has been used to type check only three large terms. I wonder how representative the benchmark is.

Questions for authors’ response
-------------------------------
1. Will the verification of the optimized version be finished for the final version of the paper?
2. Could you comment on my concern regarding macro benchmarks?
3. Why not define `Ref.t` as a tuple that include the `store`? This avoids passing around the `store` explicitly (and thus seems to make it impossible to create unchecked programming errors). It seems that you are doing this already on page 6, but not in the API on page 8.

Comments for authors
--------------------
+ page 6/7: The tree and explanation for the state after `Store.snapshot s` is missing in Fig 1.
+ page 7: There is a spurious argument `s` in `Store.set s r 2` (see also question above).
+ page 8: Does the definition of `data` depend on support for some form of existential types in OCaml? (`'a` is not bound in the return type of `Diff`). It might be worthwhile to include a remark.
+ page 8: Inconsistent spacing in `Diff(r, v,B)`.
+ page 13: I am not sure Vindum/Birkedal is the right citation for persistent assertions in Iris; they have been in Iris since the beginning (they were still called "pure" in Jung et al. 2015; the terminology persistent has been used since Jung et al. 2016). Vindum/Birkedal proposed the "persistent points-to" predicate, but that is not used here.
+ page 14: "effiectievly"
+ page 14: "ghost cell" and "ghost location" different name for same thing?
+ page 14: Use `$\mathit{Value}$` not `$Value$`, the spacing in the latter is strange.
+ page 15: overfull hbox
+ page 17: "or the code is correct" The fact that testing did not find a bug, does not mean it is correct. It might mean it is very likely correct.
+ page 19: Use consistent labels in Fig 5: "set 1" and "set 16"
+ page 20: "and their Vector is 6×" Grammar "there"?
+ page 23: "Puente [2017], [Stokke 2018]," Inconsistent use of \citet and \cite.
+ page 23: "Moine, Charguéraud and Pottier [2022] proposes the only formal verification" Why isn't this paragraph in 6.5 on formal verification?
+ page 23: Verification of transactions, if you consider concurrency there is surely more work, e.g., Chang et al. OSDI'23 in Iris.
+ page 24: "Demaine, Langerman and Price [2008] presents" should be plural "present" (same error occurs more often)
+ page 25: "context-depedent"



Review #48C
===========================================================================

Overall merit
-------------
A. Good paper, I will champion it

Reviewer Expertise
------------------
Y. I am knowledgeable in this area, but not an expert

Reviewer Confidence
-------------------
3. Good: I am reasonably sure of my assessment

Paper summary
-------------
The paper treats a very natural problem in functional programming with imperative features, namely, the impersistence of mutable state. The authors propose an OCaml library that extends the built-in state cells with a notion of persistent snapshots, and the ability of the updates to the state to be rolled back to any of the snapshots. The resulting state is still ephemeral, since there is only ever one "current" state — but this allows the library to be efficiently implemented. The crucial observation is that taking snapshots efficiently allows for a less defensive treatment of history through forgetting updates that can never be recovered.

On top of this infrastructure, the library builds a specialised infrastructure for the most commonly encountered patterns of usage. The authors build a model of (a simplified version of) their library and prove it correct using separation logic, and provide a number of benchmarks to argue for the implementation's efficiency.

Assessment of the paper
-----------------------
While the authors start their implementation from a classic technique, and the key observation seems rather straightforward, its impact appears to be far-reaching. The design of the library and the representation of its data structures prove clear and easy to understand, and the higher-level features built on top of it support the most common usage patterns. The verification effort provides a good model and enhances the understanding of the fundamental concept, and the benchmarks provide empirical data to justify that the implementation performs well in the common usage patterns (although, unsurprisingly, it does not always remain on par with custom-tailored stores). The presentation is lucid and the paper was a pleasure to read.

The only clear limitation of the practical evaluation is the fact that, as far as I can tell, *all* the benchmarks only consider transactional cases — and thus, the core snapshot features are seemingly not tested for efficiency at all. While they clearly provide additional features on top of the transactional interface, this leaves the reader wondering whether they are a practically important aspect of the library. Including a benchmark or an example where they are useful would strengthen the justification for the design of the library.

Even without the practical evaluation of the full power of persistent stores, I think this paper represents a strong contribution, and I am in favour of accepting it.

Questions for authors’ response
-------------------------------
Are you aware of any real-life problems that require the full power of persistent stores (other than the dynamic binding in LISP mentioned in the introduction)? Have you benchmarked your implementation on any such problems?

Comments for authors
--------------------
While record elision reduces the number of updates recorded for each ref cell, the diffs are still recorded per cell. Intuitively, it seems that one could push the elision further and allow for a diff-link to record multiple, essentially parallel updates to multiple cells of state. Of course, this will not change the total amount of data that needs to be saved, but maybe it could improve locality or limit the number of link traversals necessary? Have you considered a representation of this kind at all?



Review #48D
===========================================================================

Overall merit
-------------
A. Good paper, I will champion it

Reviewer Expertise
------------------
X. I am an expert in this area

Reviewer Confidence
-------------------
3. Good: I am reasonably sure of my assessment

Paper summary
-------------
This paper introduces an OCaml library, Store, that efficiently abstracts a number of well known approaches for creating snapshots of program state. Store supports creating both persistent and semi-persistent (no branching) snapshots and restoring them through an explicit API. The implementation is subtle and a Coq proof of correctness is provided for the core technique. A performance study is conducted that shows Store meets its performance goals.

Assessment of the paper
-----------------------
This paper presents its contributions very well, the writing is clear and engaging, and the result appears to be an efficient and reusable library to replace common ad-hoc patterns in compiler construction, theorem proving, and beyond.

I'm not sure the present formalization does much for readers, given the absence of record elision. I was convinced of the correctness of the method from the paper alone, so I would want a mechanized proof to convince me the _implementation_ is correct, too. Clearly, formalization was helpful in producing the implementation in the first place.

I really appreciated the performance study; it built a lot of confidence in the technique for me. In particular, the discussion of liveness was very compelling.

I think this paper is clearly above the bar. I am reminded of the classic precedence-climbing method for parsing expressions: both techniques are easy to implement (once you _get_ them), highly reusable, and efficient enough to be used outside prototyping. Are there faster persistent data structures and expression parsers? Sure, but nothing with quite the same effort-to-reward ratio. I look forward to using this in my own work.

Questions for authors’ response
-------------------------------
Will the Store library be open-sourced and added to OPAM?

The related work is quite thorough within the PL literature, but many of these problems are encountered in relational databases, too. Journaling, snapshots, rollbacks... all bread and butter for database research. I wonder if conducting a literature review there would reveal some scenarios that call for confluence (after all, concurrent transactions have to be merged)?

Comments for authors
--------------------
I'm not surprised that a sudoku solver has faster backtracking and I don't consider that fact a threat to this paper. I'm sure the same is true for chess engines, too.

Back of the envelope: a 9x9 sudoku board consists of 81 cells that have up to 10 distinct states. So 4 bits suffices per cell, for 41 bytes total. Modern CPUs have 64-byte cache lines. Both NEON and AVX-512 have single instructions that can save/restore such a snapshot. It's easy to see why it doesn't make sense to track per-cell differences when the structure is so small.

A 25x25 board is worse: 418 bytes for a packed 3x5-bit-in-16-bit representation, but you can still fit many of these in cache, within a single page.

---

Fig 6(c) has "transactions" misspelled (missing the "n").
