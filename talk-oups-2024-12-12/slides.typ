#import "@preview/touying:0.5.3": *
#import themes.metropolis: *

#show: metropolis-theme.with(
  aspect-ratio: "16-9",
  footer: self => self.info.institution,
  config-info(
    title: [Store],
    author: [
      #let face(name) = [
        #image("pictures/" + name + ".jpg", height:10em)
        #v(0.6em)
      ]
      #grid(columns: (1fr,) * 4,
        face("Clement_Allain"),
        face("Basile_Clement"),
        face("Alexandre_Moine"),
        face("Gabriel_Scherer"),
        [Clément Allain],
        [Basile Clément],
        [Alexandre Moine],
        [Gabriel Scherer],
      )
    ],
    date: datetime.today(),
    institution: [OUPS !],
  ),
)

#show link: set text(blue)

#title-slide()

// #let pause = []

= Plan

+ `opam install store` ?
+ vroum
+ `Qed.`

= `opam install store` ?

== Demo

#v(1fr)

Store lets you easily add *backtracking* to your mutable data structure. Example:

#v(1fr)

#grid(
  columns: (auto, auto),
  rows: (auto, auto),
  gutter: 1.5em,
[#pause
Union-find without Store:

```ocaml
type 'a node = 'a data ref
type 'a data =
| Link of 'a node
| Root of { rank: int; v: 'a }

val make : 'a -> 'a node
val union :
    ('a -> 'a -> 'a) ->
    'a node -> 'a node -> unit
val find : 'a node -> 'a
```
],
[#pause
Union-find with Store:

```ocaml
type 'a node = 'a data Store.Ref.t * Store.t
type 'a data =
| Link of 'a node
| Root of { rank: int; v: 'a }

val make : Store.t -> 'a -> 'a node
val union :
    ('a -> 'a -> 'a) ->
    'a node -> 'a node -> unit
val find : 'a node -> 'a
```
])

#v(1fr)

#pause
Our use-cases: (1) union-find for GADT equations. (2) Alt-Ergo ?

== API

#{
set text(size: 17pt)
grid(
  columns: (auto, auto),
  rows: (auto, auto),
  gutter: 2em,
[
```ocaml
type store

val create : unit -> store

module Ref : sig
  type 'a t
  val make : store -> 'a -> 'a t
  val get : store -> 'a t -> 'a
  val set : store -> 'a t -> 'a -> unit
end
```
],
[#pause 
```ocaml
type snapshot
val capture : store -> snapshot
val restore : store -> snapshot -> unit
```
#pause
```ocaml

type transaction
val transaction : store -> transaction
val rollback : store -> transaction -> unit
val commit : store -> transaction -> unit

```
#pause
```ocaml

val temporarily : store -> (unit -> 'a) -> 'a
val tentatively : store -> (unit -> 'a) -> 'a
```
])
}

== Usage example

=== Unifier.ml

#v(1em)

```ocaml
val unify : ('a node * 'a node) Queue.t -> 'a node -> 'a node -> unit

let unify v1 v2 =
  let q = Q.create() in
  (* Wrap unification in a transaction, so that a failed unification attempt
     does not alter the state of the unifier. *)
  try
    Store.tentatively store (fun () -> unify q v1 v2)
  with S.InconsistentConjunction ->
    raise (Unify (v1, v2))
```

= vroum

== Complexity

#v(1fr)

#let inner_stroke = (x, y) => if x > 0 and y > 0 { 1pt }

// Base template already configured tables, but we need some
// extra configuration for this table.
#{
  set table(align: (x, _) => if x == 0 { left } else { right })
  table(
    columns: (auto, 1fr, 1fr, 1fr, 1fr),
    stroke: inner_stroke,
    table.header[][`get`       ][`set`       ][`capture` ][`restore`],
    [Imperative], [$O(1)$],     [$O(1)$],     [none],     [none],
    [Map],        [$O(log n)$], [$O(log n)$], [$O(1)$],   [$O(1)$],
    [Dynarray],   [$O(1)$],     [$O(1)$],     [$O(n)$],   [$O(1)$],
    [Store],      [$O(1)$],     [$O(1)$],     [$O(1)$],   [$O(Delta)$]
  )
}

#v(1fr)

$n$: total number of elements \
$Delta$: number of elements changed between two versions

(Note: number of elements changed $<<$ number of changes)

\

#v(1fr)

== Measurements

#v(1fr)

// Base template already configured tables, but we need some
// extra configuration for this table.
#{
  set table(align: (x, _) => if x == 0 { left } else { right })
  table(
    columns: (auto, 1fr, 1fr, 1fr, 1fr),
    stroke: inner_stroke,
    table.header[][No backtracking][ratio][Backtracking][ratio],
    [Imperative], [0.21s],         [1x],  [none],       [none],
    [Map],        [0.88s],         [4x],  [0.08s],      [4x],
    [Dynarray],   [0.28s],         [1.3x],[1.3s],       [70x],
    [Store],      [0.21s],         [1x],  [0.02s],      [1x]
  )
}

#v(1fr)

Workload: type-checker, rollback on unification errors. Lots of small transactions. \
Qualitatively different workloads:
- almost no backtracking
- state is cheap to copy (sudoku)
- _lots_ of `restore` with many changes

#v(1fr)

= `Qed.`