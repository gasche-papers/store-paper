> Please take the following issues into your consideration
> (cf. instructions page).
> 
> - Please double check that you use the latest version of the LaTeX class file acmart.cls (Version >= v2.08).
>   This new version includes major changes and should be used for all ICFP papers.

done

> - Please double check that you copied exactly the paper-specific command that the instructions page prints for you.
>   The line breaks should be respected.
> 
>   Use the following commands to setup the bibliographic information correctly:
> %%% The following is specific to ICFP '24 and the paper
> %%% 'Snapshottable Stores'
> %%% by Clément Allain, Basile Clément, Alexandre Moine, and Gabriel Scherer.
> %%%
> \setcopyright{rightsretained}
> \acmDOI{10.1145/3674637}
> \acmYear{2024}
> \acmJournal{PACMPL}
> \acmVolume{8}
> \acmNumber{ICFP}
> \acmArticle{248}
> \acmMonth{8}
> \acmSubmissionID{icfp24main-p48-p}
> \received{2024-02-28}
> \received[accepted]{2024-06-18}

done

> - Title: Headline-style capitalization should be used.
>   It should be: Snapshottable Stores

done

> Please compile the document several times after these changes.
> 
> 
> - The page limit is 27 pages + 5 pages of appendix + references.
>   Page 28 should not be used for the content (acknowledgements) of the paper.

done (TODO: how many appendix pages do we have?)

> - Page 27 should be filled with the acknowledgements.
>   There is no need for a new page for any of the sections at the end of the paper.
>   Also, the references should continue right below the acknowledgements.

done

> - The abstract text that you copy and paste into the online form
>   will be shown separately from your paper in the digital library.
>   Therefore, the abstract text should be self-contained and should not use any references.
>   The abstract text that you copy into the text field during the submission process
>   should match the text on the paper.
> 
>   Please omit the reference in the abstract in both locations: On the paper and in the text field.

done

> - All levels of headings: Headline-style capitalization should be used.
>     Capitalize:
>       - first and last word, first word after a colon
>       - all major words (nouns, pronouns, verbs, adjectives, adverbs)
>     Lowercase:
>       - articles (the, a, an)
>       - prepositions (regardless of length)
>       - conjunctions (and, but, for, or, nor)
>       - to, as
>     Hyphenated Compounds:
>       - always capitalize first element
>       - lowercase second element for articles, prepositions, conjunctions
>         and if the first element is a prefix or combining form that could not stand by itself
> 
>   Please note: The first-level headings should use this capitalization as well with the new version v2.08.

done

> - Page 1: The information 'Appendices missing: A long version of this paper, with appendices, is available online.'
>   should be used as a \titlenote, not as a individual section.

done

>   Please consider submitting the appendix as additional material via the links at the bottom of the submission page.
>   Like this, it will be linked on the landing page of your paper in the digital library.

I tried to use the "Supplementary material" form on the conference-publishing website to submit a PDF with appendices, but it only allows me to submit a .zip file and expects a README.txt, LICENSE.txt etc. This does not seem designed to allow to submit complementary PDF documents. So I gave up for now, and will put the full version on arXiv.

(Note: I would like to submit the *full* paper with appendices, not just the appendices, so that cross-paper PDF references keep working.)

> - Orphans and widows should be eliminated (cf. instructions page for hints).
>   Page 8 should be corrected.

done

> - Figure 7e should not run into the margins outside the text area.

done

>   Via the link below, you can view your document with an added frame of the correct text-area size:
>   https://www.conference-publishing.com/checkPaper.php?Event=ICFP24MAIN&Paper=852f3689e432cbee62fc9152d882a6

done

> Please make the proposed changes and re-submit your paper if possible within three workdays.
> (Please use the submission link from the author-kit e-mail, do not send by e-mail.)
