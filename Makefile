FILES=store.tex store-with-appendices.tex

LATEXMK=latexmk -synctex=1 -interaction=nonstopmode -pdf -pdflatex="pdflatex %S"
.PHONY: mk
mk:
	$(LATEXMK) $(FILES)

diff:
	git-latexdiff --main store.tex --latexmk original-submission --bibtex --no-flatten -o diff.pdf

clean:
	latexmk -c
	rm -f diff.pdf
	rm -f comment.cut
	rm -f *.{log,bbl,nav,rev,snm,vrb,vtc}

.PHONY: all clean diff
