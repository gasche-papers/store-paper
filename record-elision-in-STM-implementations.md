# Do Software Transactional Memory implementations perform record elision?

It looks like STM algorithms need to track both the 'before' and the
'after' value for all modifications, to be able to reason about
consistency at commit time. In particular, all modifications inside an
STM transaction need to be logged. If a previous log entry exists for
the location, it can be reused and modified to point to the new
'current' value, but there is still work to do, unlike with record
elision.

kcas (OCaml):
  https://github.com/ocaml-multicore/kcas/blob/5f3a39dfc72189e2b83f96c3754d402d5e7d6bc5/src/kcas/kcas.ml#L704-L720
  
  Not sure what the `is_cmp` test means, but both branches perform
  some form of logging that tracks the updated value. They only elide
  changes when `before == after` -- when the new value is the same as
  the old value.
  
STM (Haskell):
  pure Haskell implementation (when compiling with non-GHC compiler, that is never)
     see writeTVar in
       https://hackage.haskell.org/package/stm-2.5.3.0/docs/src/Control.Sequential.STM.html#writeTVar
     the line
       `modifyIORef r (writeIORef ref oldval >>)`
     logs an "undo write" in the record `r` unconditionally
     (this is a `IO ()` value that accumulates computation to perform;
      I guess that it is forced if/when the transaction is aborted.)
 C implementation GHC:
   https://github.com/ghc/ghc/blob/5121a4edd216491095ec09790a1535105e1c9006/rts/STM.c#L1349-L1387
   an "entry" is looked up (note: entry lookup seems linear in the `trec` (transaction record) size??),
   created if not found, but updated if found (so: no full elision, but slightly less work)

ZIO STM (Scala);
  https://github.com/zio/zio/blob/ecb38a3bf15b085080f9c092dbbd88091f5ebb32/core/shared/src/main/scala/zio/stm/TRef.scala#L96-L104
  
vMVCC (Go)
  a STM library implemented in Go, with a verification in Iris
  paper: https://iris-project.org/pdfs/2023-osdi-vmvcc.pdf
  code: https://github.com/mit-pdos/vmvcc
  in particuler, the key structure is the "write buffeR": https://github.com/mit-pdos/vmvcc/blob/116f2a360d4390896cf042547caf757ab881e02a/wrbuf/wrbuf.go
  (same implementation style as the other, a per-transaction log with linear search)

Notice how most of those implementations use a linear search, except for kcas with a highly optimized splay tree, which probably fares better on large transactions. We don't need a linear search to make record-elision decisions, because we are a sequential library with exactly one most recent transaction active at the same time, and we can store the per-transaction state in each reference directly.

