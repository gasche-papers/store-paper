Note: this file is imported from
  https://gitlab.com/gasche/store/-/blob/bench-analysis/bench/README.md
current commit 20ad772f.

A description of the benchmarks in <./bench.ml> and their results.

# The benchmarks

## Implementations

The interfaces that implementations may provide are listed in <./sig.ml>.

- `Store` (<./StoreBasile.ml>): our implementation in the present `store` library.

- `Ref` (<./StoreRef.ml>): native OCaml references (no support for backtracking of any kind)

   This is the gold standard for "raw" get/set operations.

- `TransactionalRef` (<./StoreTransactionalRef.ml>): a store by François Pottier in [union-find](TODO), that only supports *non-nested* transactions.

   We expect to be at least as fast, despite supporting more features.

- `BacktrackingRef` (<./StoreBacktrackingRef.ml>): a store by Gabriel Scherer, documented in <https://hal.science/hal-03936704>, which only supports a semi-persistent interface, not fully persistent snapshots.

   We expect to be at least as fast, despite supporting more features.

- `Facile` (<./StoreFacile.ml>, <./vendor/facile/fcl_stak.ml>): the backtrackable references of the [FaCiLe](http://facile.recherche.enac.fr/) library, a well-established constraint-programming library. They support a semi-persistent interface, not fully persistent snapshots.

   We expect to be at least as fast, despite supporting more features.

- `Map` (<./StoreMap.ml>): an implementation using persistent maps (from the OCaml standard library), also provided by the `union-find` library.

   We expect it to have very slow get/set operations (O(log n) instead of O(1)), but instantaneous capture/restore operations (O(1)).

- `Vector` (<./StoreVector.ml>): an implementation using dynamic arrays, also provided by the `union-find` library.

   We expect it to have fast get/set operations (O(1)), but very slow capture/restore operations (O(n)).

## Benchmark parameters

All benchmarks are purely synthetic, and they are parametrized by the following environment variables.

- ROUNDS: the benchmark does *something* in a loop, ROUNDS time; the total time should scale linearly with this variable (but this may not be just a for loop, there may be an environment growing from one round to the next)
  
- NCREATE, NREAD, NWRITE: the logarithm of the number of references to {create,read,write} each round. For example, using NCREATE=8, NREAD=15, NWRITE=10 will create 256 references, perform 1024 writes (spread evenly between them) and 32K reads. (Real-world workloads tend to have noticeably more reads than writes.)

## Benchmarks

- `Raw`: measuring just `get` and `set` on references, without any snapshots involved.
   
   We consider that these are the critical operations for our workloads where snapshots are rare relatively to read and writes.
  
- `Transactional-{raw,full}`: uses non-nested transactions.

   The `raw` variant is in fact the `Raw` benchmark, but running under the transaction -- some (semi-)persistent implementations could have a dedicated fast path when no snapshot or transaction has been captured, and in this case this benchmark may be more representative than `Raw` of observed performance in presence of a few snapshots.
   
   The `full` variant runs one transaction for each round of the benchmark, which allows to increase the frequency of (non-nested) transactions relative to the amount of read/write.
   
- `Backtracking-{abort,commit,persistent}`: a backtracking workload using nested transactions -- the nesting depth is the number of rounds of the benchmark.

   The `abort` variant aborts each transaction, that is, reverts all changes when the transaction terminates.
   
   The `commit` variant commits each transaction, that is, preserves all changes when the transaction terminates.
   
   The `persistent` variant is equivalent to `abort`, but it uses the persistent API rather than the semi-persistent API, so we expect it to be (slightly) slower: on abort, both versions have to walk back the history to revert all changes, but the persistent version also has to update the history itself to allow "going back in the other direction" again, while the semi-persistent versions simply throw the history away. Comparing the `abort` and `persistent` benchmarks is thus a way to measure the interest of providing a more complex semi-persistent API, rather than just the persistent interface.

## Controlling the quality of the benchmarks

### Benchmark code

`get` and `set` are extremely fast operations, so it is easy for the benchmarking setup to introduce auxiliary costs that dominate the performance comparison. For example, we want to know whether the benchmark has those as indirect calls, direct calls, or even inlined -- we want to ensure that all implementations use the same approach if possible, and think about what is more likely to happen in real-world user software.

The only way we know to achieve this is to inspect the -dcmm output produced by the compiler for the benchmark script. The <./dune> file passes the `-dcmm` flag, and has a comment with the location of the resulting output (on my machine, with my Dune version, this month).

To have better control over this without duplicating a lot of code, we made the choice to use `[@inline]` directives in the benchmark code and enforce flambda. Hopefully this does not lead us to making performance deductions that would not hold on non-flambda setups.

### Measurement noise

The noise / speed variations of running a given binary several times in the same environment are well accounted-for by the `hyperfine` tool we use for benchmarking: it runs each test many times, and reports confidence intervals for its measurements. 

Many benchmarks report noise below 1%: they are running on a machine setup for reproductible performance (more precisely, hyperthreading is disabled, frequency scaling is disabled, the "performance" governor is used, and the benchmarks run on an isolated CPU).

### Known measurement biases

We call measurement "biases" the source of performance variability that cannot be easily observed by just running the same binary several time -- in particular, they are *not* included in the confidence intervals. We don't know of a robust way to eliminate all of them. Our approach has been to study carefully each benchmark result that we found surprising, which more often than not revealed a form of measurement bias. We found the following issues:

- Minor bugs in the benchmarking code, now fixed (not shown in the result).

- `get` and `set` for the different implementations were inconsistently inlined, resulting in unfair comparisons. We modified each implementation to consistently inline `get` and the "fast path" of `set` (when a form of record elision is possible), and de-inline the "slow path" of `set`.

- On some occasions we applied minor optimizations to the benchmarked variants when easy to do so (for example, we removed an extra load from the fast path of `Facile` that was only necessary in the slow path, and we removed an optimization when writing the current value that some implementations had but not others).

- The worst source of variability we found was due to code alignment issues. On our main benchmarking machine (less so on others), code alignment issues can create noticeable performance swings, typically around 10%. We noticed because changing a part of the code would affect the performance of unrelated implementations. See below for a full explanation of our strategy to reduce this bias.

### Controlling for code-alignment effects

By default, we build 16 different versions of the benchmark binary, which differ by irrelevant constants being placed at the beginning of the binary to change the position of the benchmarked routines in the binary. We run each implementation on all 16 binaries, and typically observe a bimodal distribution -- a few benchmarks have three modes or more -- where each mode is very concentrated.

By default we only report the "fast" mode of each implementation, that is, its performance reached with "favorable" alignments. Specifically for the `Facile` benchmark we observe some slight inconsistencies (it performs 3% slower than `Store` in some case where would not expect it) that are not present in the "slow" mode, that is, when comparing all implementations on "unfavorable" alignments. Whenever we observe such a difference, we report two numbers: the mean runtime under the best alignment, and the mean runtime under the worst alignment.


# Benchmark results

These results were collected by running the <./benchmarks.sh> script in the current directory. The raw results are produced in the <./results/> subdirectory, but we currently do not version them.

Our recommendation to update the numbers below is to *not* try to update the numbers on every run of the benchmarks, but only if the qualitative analysis changes for this benchmark. (It is okay if we only update the numbers for one benchmark, and not for another benchmark, except in a couple case where we compare absolute numbers between two benchmarks in the analysis.)


## Raw

`Ref` is the gold standard for this benchmark. `Store`, `BacktrackingRef` and `Facile` have a small overhead (2%-6%). `TransactionalRef` is a bit slower (12-13% overhead), `Vector` is slower (63-67% overhead), and `Map` is much slower (25x-27x).

| (Marvin) Command   | Fast alignment [ms] |     Relative | Slow alignment [ms] |     Relative |
|:-------------------|--------------------:|-------------:|--------------------:|-------------:|
| `Ref`              |         77.7 ± 0.10 |         1.00 |         86.6 ± 0.50 |         1.00 |
| `BacktrackingRef`  |         79.4 ± 0.64 |  1.02 ± 0.01 |         88.3 ± 0.10 |  1.02 ± 0.01 |
| `Store`            |         80.7 ± 0.76 |  1.04 ± 0.01 |         89.3 ± 0.03 |  1.03 ± 0.01 |
| `Facile`           |         82.3 ± 0.18 |  1.06 ± 0.00 |         89.1 ± 0.04 |  1.03 ± 0.01 |
| `TransactionalRef` |         88.1 ± 0.09 |  1.13 ± 0.00 |         97.1 ± 0.04 |  1.12 ± 0.01 |
| `Vector`           |        126.5 ± 0.02 |  1.63 ± 0.00 |        144.6 ± 0.02 |  1.67 ± 0.01 |
| `Map`              |       2126.3 ± 4.61 | 27.36 ± 0.07 |       2200.9 ± 7.48 | 25.43 ± 0.17 |

Notes:

- We expect `Facile` to have the same performance as `Store` here due to the similarity in their implementations. Both implementations have bimodal distributions across alignments, with the same *slow alignment* performance, but `Facile` somehow has a slightly slower *fast alignment* performance. Generally we report the fast alignment results only, but in this case we feel that the slow alignment results more fairly represent the relative comparison between implementations. (Whenever that happens, we will show the data for both.)

- We believe that `Vector` is slower due to the extra indirection and possibly the bound check.

- `TransactionalRef` has a slower `set` operation in absence of backtracking (two polymorphic writes instead of one), but it keeps the same code in presence of backtracking (thanks to its restriction to non-nested transactions), so it will perform better (relatively to BacktrackingRef, Facile, Store) in the Transactional benchmarks that follow.

### Get

If we measure only the `get` operation, all careful implementations have exactly the same performance -- they perform a single memory read. Again `Facile` is slightly slower under fast alignment, but exactly-as-fast as expectded under slow alignment.

| (Marvin) Command   | Fast alignment [ms] |     Relative | Slow alignment [ms] |     Relative |
|:-------------------|--------------------:|-------------:|--------------------:|-------------:|
| `Ref`              |       128.1 ±  0.04 |         1.00 |       146.2 ±  0.02 |  1.00 ± 0.00 |
| `BacktrackingRef`  |       128.2 ±  0.03 |  1.00 ± 0.00 |       146.2 ±  0.02 |         1.00 |
| `TransactionalRef` |       128.2 ±  0.05 |  1.00 ± 0.00 |       146.2 ±  0.02 |  1.00 ± 0.00 |
| `Store`            |       128.2 ±  0.03 |  1.00 ± 0.00 |       146.2 ±  0.02 |  1.00 ± 0.00 |
| `Facile`           |       132.2 ±  0.49 |  1.03 ± 0.00 |       146.2 ±  0.01 |  1.00 ± 0.00 |
| `Vector`           |       218.9 ±  0.04 |  1.71 ± 0.00 |       255.2 ±  0.02 |  1.75 ± 0.00 |
| `Map`              |      3467.1 ± 67.28 | 27.07 ± 0.53 |      3954.6 ± 21.18 | 27.05 ± 0.14 |

### Set1

Again `BacktrackingRef`, `Facile`, `Store` are in the same performance ballpark, with `Vector` also doing just as well. `TransactionalRef` is slower (2 polymorphic writes, as we explained in the `Raw` benchmark discussion), and `Map` is much slower (15x).

Note: the timings are *very small*, it is hard to trust the ratios.

| (Marvin) Command   |   Mean [ms] |     Relative |
|:-------------------|------------:|-------------:|
| `Ref`              |  2.4 ± 0.10 |         1.00 |
| `BacktrackingRef`  |  2.6 ± 0.07 |  1.10 ± 0.05 |
| `Facile`           |  2.7 ± 0.07 |  1.14 ± 0.06 |
| `Store`            |  2.8 ± 0.06 |  1.15 ± 0.06 |
| `Vector`           |  2.8 ± 0.03 |  1.17 ± 0.05 |
| `TransactionalRef` |  3.7 ± 0.04 |  1.56 ± 0.07 |
| `Map`              | 38.0 ± 0.23 | 15.86 ± 0.69 |

## Transactional-raw

The results of the Transactional-raw benchmark (which is similar to Raw, except all creations and accesses are performed under one transaction) are relatively similar to the Raw benchmarks, except that `TransactionalRef` is back in the head pack. This is probably because its `set` implementation is faster in the slow path than (semi)-persistent versions.

| (Marvin) Command   | Fast alignment [ms] |    Relative | Slow alignment [ms] |    Relative |
|:-------------------|--------------------:|------------:|--------------------:|------------:|
| `Store`            |        100.6 ± 1.31 |        1.00 |        111.5 ± 0.03 |        1.00 |
| `BacktrackingRef`  |        105.7 ± 0.04 | 1.05 ± 0.01 |        116.9 ± 0.04 | 1.05 ± 0.00 |
| `TransactionalRef` |        106.4 ± 0.10 | 1.06 ± 0.01 |        117.7 ± 0.23 | 1.06 ± 0.00 |
| `Facile`           |        106.9 ± 0.25 | 1.06 ± 0.01 |        115.3 ± 0.04 | 1.03 ± 0.00 |
| `Vector`           |        157.9 ± 0.04 | 1.57 ± 0.02 |        180.6 ± 0.03 | 1.62 ± 0.00 |

### Get

Same behavior as Raw-Get.

| (Marvin) Command   | Fast alignment [ms] |    Relative | Slow alignment [ms] |    Relative |
|:-------------------|--------------------:|------------:|--------------------:|------------:|
| `BacktrackingRef`  |        128.2 ± 0.03 |        1.00 |        146.2 ± 0.02 | 1.00 ± 0.00 |
| `Store`            |        128.3 ± 0.69 | 1.00 ± 0.01 |        146.2 ± 0.02 | 1.00 ± 0.00 |
| `TransactionalRef` |        128.4 ± 0.94 | 1.00 ± 0.01 |        146.2 ± 0.02 |        1.00 |
| `Facile`           |        132.0 ± 0.36 | 1.03 ± 0.00 |        146.2 ± 0.03 | 1.00 ± 0.00 |
| `Vector`           |        218.9 ± 0.02 | 1.71 ± 0.00 |        255.2 ± 0.03 | 1.75 ± 0.00 |

### Set1

This benchmark does a single `set` per reference per round, so it does not benefit from record elision.

| (Marvin) Command   |   Mean [ms] |    Relative |
|:-------------------|------------:|------------:|
| `Store`            | 32.7 ± 0.13 |        1.00 |
| `Vector`           | 34.4 ± 0.07 | 1.05 ± 0.00 |
| `Facile`           | 38.8 ± 0.13 | 1.19 ± 0.01 |
| `BacktrackingRef`  | 41.4 ± 0.12 | 1.27 ± 0.01 |
| `TransactionalRef` | 42.5 ± 0.11 | 1.30 ± 0.01 |

`Vector` shines on this benchmark, which sets each reference exactly once. `Vector` performs a full copy of the vector, and then sets each element efficiently. All other implementations record each modification (no record elision), which corresponds to an on-demand copy of each element, which is bound to be noticeably slower than a batch copy.

### Set16

This benchmark does 16 `set` per reference per round, so it does benefit from record elision.

This benchmark use a smaller number of references than Set1 (2^10 references instead of 2^16). In consequence, `Vector` also improves, as its copying of all references is more efficient.

| (Marvin) Command   |    Mean [ms] |    Relative |
|:-------------------|-------------:|------------:|
| `Store`            |  80.5 ± 0.33 |        1.00 |
| `Vector`           |  84.7 ± 0.10 | 1.05 ± 0.00 |
| `Facile`           |  95.8 ± 0.16 | 1.19 ± 0.01 |
| `BacktrackingRef`  | 102.2 ± 0.29 | 1.27 ± 0.01 |
| `TransactionalRef` | 105.0 ± 0.20 | 1.31 ± 0.01 |

Note: the benefits of record elision differ for each implementation, depending on the efficiency of the elision check:

- `Store` performs two loads and an integer comparison.
- `Facile` performs 3 loads and 1 comparison. Compared to `Store`, this seems to add an overhead of about 1ns / 1 CPU cycle per write.
- `BacktrackingRef` performs 3 loads and two comparisons. Compared to `Facile`, this seems to add an extra overhead of about 0.5ns / 2 CPU cycles per write.


## Transactional-full

The results are as expected. `Vector` is doing worse than on the `Transactional-Raw` benchmark, but the results cannot be compared directly as the worklaods are different. The relative performance of `Facile` is disappointing. It looks like its backtracking implementation is somewhat slow: backtracking each write incurs an indirect call.

| (Marvin) Command   |    Mean [ms] |    Relative |
|:-------------------|-------------:|------------:|
| `Store`            | 138.6 ± 1.48 |        1.00 |
| `TransactionalRef` | 140.5 ± 0.42 | 1.01 ± 0.01 |
| `BacktrackingRef`  | 141.2 ± 1.62 | 1.02 ± 0.02 |
| `Vector`           | 193.4 ± 0.06 | 1.40 ± 0.01 |
| `Facile`           | 196.5 ± 0.32 | 1.42 ± 0.02 |

### Get

(as expected)

| (Marvin) Command   | Fast alignment [ms] |    Relative | Slow alignment [ms] |    Relative |
|:-------------------|--------------------:|------------:|--------------------:|------------:|
| `TransactionalRef` |        128.5 ± 0.04 |        1.00 |        146.6 ± 0.02 |        1.00 |
| `BacktrackingRef`  |        128.6 ± 0.03 | 1.00 ± 0.00 |        146.6 ± 0.02 | 1.00 ± 0.00 |
| `Store`            |        128.6 ± 0.05 | 1.00 ± 0.00 |        146.7 ± 0.02 | 1.00 ± 0.00 |
| `Facile`           |        132.3 ± 0.05 | 1.03 ± 0.00 |        146.6 ± 0.03 | 1.00 ± 0.00 |
| `Vector`           |        220.8 ± 0.02 | 1.72 ± 0.00 |        257.3 ± 0.03 | 1.75 ± 0.00 |

### Set1

Vector shines. Facile is very slow, probably due to the indirect call.

| (Marvin) Command   |     Mean [ms] |    Relative |
|:-------------------|--------------:|------------:|
| `Vector`           | 104.0 ±  0.31 |        1.00 |
| `TransactionalRef` | 208.1 ±  5.72 | 2.00 ± 0.06 |
| `Store`            | 257.6 ± 10.91 | 2.48 ± 0.11 |
| `BacktrackingRef`  | 260.5 ±  2.47 | 2.50 ± 0.02 |
| `Facile`           | 870.4 ±  0.98 | 8.37 ± 0.03 |

### Set16

Same analysis as Transactional-raw.Set16.

| (Marvin) Command   |    Mean [ms] |    Relative |
|:-------------------|-------------:|------------:|
| `Vector`           | 105.1 ± 0.05 |        1.00 |
| `Store`            | 116.8 ± 1.83 | 1.11 ± 0.02 |
| `BacktrackingRef`  | 121.9 ± 1.01 | 1.16 ± 0.01 |
| `TransactionalRef` | 138.8 ± 0.68 | 1.32 ± 0.01 |
| `Facile`           | 181.8 ± 0.29 | 1.73 ± 0.00 |

## Backtracking-abort

This is a backtracking-is-rare workload where we write to a thousand
references for each capture/restore pair. We expect `Vector` to be fast, since
all references need to be backtracked, and `Map` to be slow due to the
benchmark being dominated by read/writes.

Note [Basile]: The `Backtracking-abort` scenario is a bit misleading because we do
all the work then abort all levels; no backtracking (in the sense of trying
some path then trying another path when it fails, etc.) actually takes place.

| (Marvin) Command  |     Mean [ms] |     Relative |
|:------------------|--------------:|-------------:|
| `Vector`          |  164.2 ± 0.05 |         1.00 |
| `Store`           |  211.1 ± 0.71 |  1.29 ± 0.00 |
| `Facile`          |  224.5 ± 0.74 |  1.37 ± 0.00 |
| `BacktrackingRef` |  235.5 ± 0.99 |  1.43 ± 0.01 |
| `Map`             | 2633.6 ± 6.60 | 16.04 ± 0.04 |

## Backtracking-commit

The results are similar to Backtracking-abort, except that `BacktrackingRef` jumps ahead in the rankings: its implementation of `commit` is O(1) in the regime where no `abort` takes place, whereas `Store` is O(Δ) in any case.

| (Marvin) Command  |      Mean [ms] |     Relative |
|:------------------|---------------:|-------------:|
| `Vector`          |  164.1 ±  0.12 |         1.00 |
| `BacktrackingRef` |  208.3 ±  1.36 |  1.27 ± 0.01 |
| `Store`           |  209.4 ±  1.07 |  1.28 ± 0.01 |
| `Facile`          |  218.3 ±  1.68 |  1.33 ± 0.01 |
| `Map`             | 2628.0 ± 10.59 | 16.02 ± 0.07 |

## Semi-persistent vs. persistent API

On this get/set-heavy workload, the semi-persistent implementation of `Store` is noticeably faster.

| (Marvin) Command          |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `Backtracking-abort`      | 210.8 ± 1.07 |        1.00 |
| `Backtracking-persistent` | 315.9 ± 0.80 | 1.50 ± 0.01 |

Note: these results are sensitive to cache/locality effects, and in particular we observe important variations when running the benchmarks on different machines -- the relative performance difference can be smaller than observed here.

## Capture-heavy workloads

All tests above (except the Get/Set1/Set16 variants) use benchmark parameters `NCREATE=10 NWRITE=12 NREAD=16`, with about 1K references created, 4K references modified and 64K references read between each capture/restore operation.

The results below re-run some of the benchmarks in a capture-heavy workload, with parameters `NCREATE=2 NWRITE=4 NREAD=6`: 4 references created, 16 writes and 64 reads per capture/restore pair.

### Transactional-full (capture-heavy)

As expected, `Map` fares much better on capture-heavy workloads, but it remains non-competitive.

(Note [Basile]: We do not run `Map` on the non-capture-heavy workloads?)

`TransactionalRef` is slightly faster than the backtracking implementations on this workload. Its implementations of `commit` and `rollback` take advantage of the fact that nested transactions are not supported.

| (Marvin) Command   |    Mean [ms] |    Relative |
|:-------------------|-------------:|------------:|
| `TransactionalRef` |  82.4 ± 0.58 |        1.00 |
| `Store`            |  86.4 ± 1.19 | 1.05 ± 0.02 |
| `BacktrackingRef`  |  86.6 ± 1.03 | 1.05 ± 0.01 |
| `Vector`           | 100.0 ± 1.20 | 1.21 ± 0.02 |
| `Facile`           | 141.4 ± 0.31 | 1.72 ± 0.01 |
| `Map`              | 165.7 ± 1.73 | 2.01 ± 0.03 |


#### Large-support variant

The numbers above test a capture-heavy variant with a small number of references, and a small number of `set` calls per round. We also measure a "large-support" variant with a larger number of references, only a small subset of which are written on each round -- this is a more common capture-heavy workload. We can notice several interesting things:

 - `Vector` becomes much slower since it now makes many unnecessary copies;
 - `Map` becomes much slower due to read/write costs scaling with the number of references;
 - The gap between `TransactionalRef` and the backtracking implementations widens, possibly due to worse locality of the large support;
 - `BacktrackingRef` jumps ahead in the rankings, thanks to its optimized `commit` implementation.

TODO: We don't know why `Facile` becomes noticeably slower. Locality effects?

Note [Basile]: it is interesting to note that if we split `Transactional-full` into a `commit`-only and a `rollback`-only portion, `BacktrackingRef` and `Store` have similar performances on the `rollback`-only portion, but `BacktrackingRef` is faster on the `commit` portion. It is also interesting to note that if we wrap the whole thing in a toplevel transaction, the performance of `Store` does not change significantly, while `BacktrackingRef` becomes about 2x slower due to elevated memory pressure. I think that this property -- `Store` is "history oblivious" in the sense that it will have similar behavior/performance at toplevel and within a transaction -- is worth noting. `BacktrackingRef` could have made the same choice, but it didn't; it is interesting (to me at least) to notice the implications of these choices in benchmarks.

| (Marvin) Command   |    Mean [ms] |    Relative |
|:-------------------|-------------:|------------:|
| `TransactionalRef` | 115.7 ± 0.92 |        1.00 |
| `BacktrackingRef`  | 129.5 ± 1.93 | 1.12 ± 0.02 |
| `Store`            | 139.6 ± 2.90 | 1.21 ± 0.03 |
| `Facile`           | 359.4 ± 2.08 | 3.11 ± 0.03 |
| `Map`              | 587.6 ± 6.40 | 5.08 ± 0.07 |
| `Vector`           | 844.4 ± 0.69 | 7.30 ± 0.06 |


### Semi-persistent vs persistent API (capture-heavy)

On this workload we observe a 16% overhead for the persistent API compared to the optimized implementation allowed by the non-persistent API. We were expecting a larger difference; our current hypothesis is that everything fits in the cache for this workload, dampening the performance difference between implementations. (The non-capture-heavy worklow has a larger workset which overflows the cache, and shows larger differences on our benchmarking machine.)

| (Marvin) Command          |    Mean [ms] |    Relative |
|:--------------------------|-------------:|------------:|
| `Backtracking-abort`      | 245.0 ± 0.89 |        1.00 |
| `Backtracking-persistent` | 283.0 ± 1.00 | 1.16 ± 0.01 |

Note: we believe that the semi-persistent implementation provides other benefits -- for example record compression on `commit` -- that are hard to measure in performance microbenchmarks.
