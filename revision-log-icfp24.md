# Revision description

Here is the reviewer' summary of the revised suggestions:

> For the revision we ask the authors to implement the following changes:
> 
> [Mandatory] Include the additional macro benchmark in Section 5.3.
> [Recommended] Include the mechanization of record elision. If you cannot do this in time, include a discussion of the sticky issues instead.

A high-level summary of the changes is as follows:

1. We made many small changes based on proof-reading feedback from the reviewers (thanks!) and further feedback we received offline.

2. [Mandatory] We described the additional macro benchmark, briefly in the main content of the article and with more details in the appendix. We made the benchmark code publicly available and wrote a small (independent) technical report describing that work; we will refer to them in the final version of the article, but the mentions are elided in our revised version as they would be de-anonymizing.

3. [Recommended] We finished the formalization of record elision, which required an overhaul of the proof (more details below). We modified the corresponding section of the article accordingly.

  We include an archive of the new Coq/Rocq development with this response. The contents of the archive is *not* anonymized (our previous complementary artifacts were anonymous), due to lack of time to do a proper anonymization/sanitization work. The relevant part of the development is theories/persistent/pstore_2.v (this is part of a monorepo of somewhat-related Iris developments).
  
4. We did a full rewrite of the section on the interaction between persistent snapshots and semi-persistent transactions, based on your feedback that it was too hard to understand.

5. We re-ran our microbenchmarks and made some improvements. We found a mistake in our measurements of the Facile library, the actual performance is better than what we had previously measured and discussed; we updated the results accordingly.

A "latexdiff"-produced diff is included with our revision.

Meta note: the work suggested by our reviewers for this revision was important, sensibly more than our previous experiences with the PACMPL two-stage process. We welcome this shift to a more authentic journal-publishing experience, in our case it certainly aligned with our opinion of how to improve the work. Maybe in the future more time could be allocated for revisions.

## Details on Facile performance

Reminder: Facile is one of the third-party implementation that we compared against. It is semi-persistent, was implemented in performance in mind, and contains a form of record elision -- record elision is relatively simple in semi-persistent implementations, our main contribution in this respect is to show that it can also be performed in a persistent setting. 

We made a mistake in the way we implemented the Store API on top of Facile backtracking facilties for our microbenchmarks. Our performance results for Facile were worse than they should be; once fixed, their implementation is competitive with ours or better, but it has substantially worse memory-liveness properties -- a different space complexity on some realistic usage workflows such as Inferno.

We find it notable that, despite our best efforts, benchmarks and performance analysis can easily suffer from such issues. (We have already found and fixed many small issues in our benchmarking apparatus ahead of the paper writing and submission.) It is very difficult to test that the implementation of benchmarks enables a fair comparison between approaches, free from measurement biases endangering the qualitative analysis of the results. (The only effective methodology we know of is to verify each explanatory hypothesis by changing the code to falsify it; we have tried to follow this general approach in our work.)

Meta note: we did not follow the artifact evaluation process this year, due to lack of time on our side to prepare an artifact in the given timeline. We are overall very positive about the opportunities of artifact evaluation in our research community, and have participated substantially to artifact evaluation committees in previous years -- it helps in raising the quality of research software, encouraging the code to follow best practices, be documented well and packaged appropriately. However, we are skeptical that artifact evaluation would have caught this issue or similar weaknesses. In our experience, artifact evaluators verify that they can reproduce the benchmark *results* locally, but they do not have the time and means to review the benchmark *code* and to experiment at the level of detail that would reveal measurement biases or subtle implementation mistakes.


## Details on the proof of record elision

The proof of record elision is substantially harder than the verification we had included in our submission. We spent substantially more human-hours working on it than on the rest of the Rocq development.

(We were already aware of this: we initially started the formalization work with record elision in mind, but did not manage to prove its correctness in time for the submission deadline.)

Even at an informal level, reasoning on record elision is difficult: it requires understanding precisely what generations mean, and then noticing that the record-elision optimization modifies the model of existing (non-captured) nodes in the version-tree, something that never happens in absence of record elision. (The informal meaning of generations that we gave is that it counts the number of snapshots in the history of the node. But snapshots are not materialized in memory, they do not create nodes like transactions do, so defining this precisely is in fact relatively subtle.)

We considered two approaches in mind to prove record elision:
- an incremental approach, based on gradually strengthening the invariant we had already written with more and more information to be able to talk about record elision
- a clean-slate approach, based on writing a substantially different invariant, working on a different notion of version-trees that only contains captured nodes, with "chains of changes" between those nodes.

We were not sure which approach would lead to a succesful proof, and worked on both approaches in parallel. In the end we only succeeded with the "clean-slate" proof -- yesterday.
