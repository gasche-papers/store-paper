> Dans l'ensemble, surtout un tas de points mineurs. Ma
> principale question intéressante peut-être, c'est que
> la spec en logique de séparation (Figure 2) introduit
> un prédicat `store` monolithique. Par conséquent
> l'utilisateur qui choisit d'utiliser `store` plutôt
> que les références primitives perd (beaucoup) en confort
> au niveau du raisonnement; et une structure prouvée
> correcte avec des références primitives ne se porte
> pas facilement en une structure prouvée correcte
> au-dessus de `store`. Est-ce qu'il serait concevable
> que l'API de `store` offre une forme de prédicat points-to
> pour raisonner à grains fins sur les références (tout en
> conservant bien sûr la possibilité de faire capture/restore)?
> 
> 
> ---
> 

> page 1, "representant" → "representative"? (also page 24)

done

> page 2, "from the 90s" → you seem to (imlicitly) diminish the merit
> of this paper because it is old.

done

> page 3, "while performing well in all situations: snapshots, easy and cheap".
> I don't understand what this means. After the colon I expect a list of
> situations, but "snapshots, easy and cheap" does not seem to be a list of
> situations, and it is heterogeneous ("snapshots" is a name; "easy" and "cheap"
> are adjectives).

reformulated with different emphasis

> page 4 and earlier, addressing the reader (and user) as "you" seems inelegant,
> but that is just MHO.

no change for now (I find this formulation rather lightweight)

> page 4, "Store can be used": the name of the library should appear in a
> different font, otherwise the sentence is unclear.

done (textsf)

> page 4, "as is idiomatic to implement" : missing word?
> page 4, "(functional" : missing capital "F".

done


> page 4, "Splay-tree data structure" : citation?
> 
> page 4, "semi-persistence" : citation?
TODO

> page 6, "persistent arrays approaches" → "approaches to persistent arrays"?
> page 7, "will create", "will reroot" : writing in the present tense is usually recommended.
done

> page 7, "has changed directions" → "has changed direction"?
> 
> page 7, "a parent relation that sends" : a relation does not send anything.
> 
> page 7, "nodes to their parent" → "that maps/relates a node to its parent".
> 
> page 7, "when the store was first populated" : not clear what this means.
>
> page 7, "A node A models a functional mapping" : the way this is phrased,
> it sounds as if the node is the model of the mapping.
> 
> page 9, "It helps to define a model of the store and the nodes in the version
> tree" : do you associate a model with each node (only), or (also) a model with
> the store?
done

> page 9, "Before the call, n points to ..." : slightly confusing, since the
> pointer to the source code points to the point *after* the call.
done

> page 9, "At this point, the current model is ..." : the current model of
> what?
done

> page 9, "gets assigned" : ugh.
done

> page 9, "sets the data" : it was not immediately obvious to me that the
> operation that "sets the data" is the assignment " := ".
>
> page 9, "setting Mem" → "writing Mem"?
done

> page 9, "whose precondition is conditioned" : what does this mean?.
removed

> page 9, "Our implementation uses a tail-recursive variant" : what is the
> motivation for doing so? Is it more efficient?
>
> page 11, "gets mutated" : ugh.
>
> page 11, "non-elided sets" : change font in `set`.
>
> page 11, "perform an extra write and two allocations" : I assume that the two
> allocations can be coalesced? so they count as one allocation.
done

> page 11, "the number of different memory locations" → "the number of distinct memory locations"
> 
> page 11, "which is much more commonly (but not always) bounded" : could remove this part.
done

> page 11, "Preserving uniqueness in the persistence setting would be an
> instance of the order maintenance problem" : I don't see what you mean.
I don't think that we have the space to say much more about this than
"go look it up elsewhere", so I propose to not change anything here.

> page 12, "Can the memory corresponding to this snapshot be collected, or is it
> kept alive by the global Store data structure?". A different but related question
> is, once the snapshot has been dropped (collected), can the Store data structure
> perform record elision just as if this snapshot had never existed? (Answered on
> line 557, I think.)
no change

> page 12, "On the other hand, if the user forgets both the reference and all
> the snapshots mentioning them, then they". What do "them" and "they" refer
> to here?
done

> page 12, "fat nodes" : citation?
TODO

> page 12, "without record elision and transactions" → "without record elision or transactions"
done

> Figure 2, would it be conceivable to offer fine-grained points-to assertions
> as opposed to a monolithic assertion `store s σ`?
This became a full sub-thread of discussion with François, which is probably going to stay out of the present paper.

> page 13, "We write σ(r) the value" → "for the value", I think.
done

> page 13, "the assertion snapshot 𝑠 𝑡 𝜎 is persistent" : why cite Vindum and
> Birkedal here?
TODO for iris people

> page 13, "entailement"
done

> page 14, "It existentially quantifies over ...". I would suggest splitting
> this sentence; it is too long, and it is somewhat ambiguous. E.g., in "the
> labeled graph g, a set of edges from node to node labeled with the pair of a
> reference and its old value," it was not immediately obvious to me that the
> definition of a graph is inlined in the sentence at this point.
TODO for Alexandre

> page 14, "with the pair" : "with a pair"?
done

> "its old value" : what do you mean?
TODO

> page 14, "coming after" → "that follows"
> 
> page 14, "effiectievly"
> 
> page 14, "and that if" → remove "that"
> 
> page 14, "that we describe below" → "which we describe below".
> 
> page 14, "snapshosts"
> 
> page 14, "Notice that this is not a map" : I suggest changing to
> "Notice that C is not a map". Otherwise, the reader may confuse M and C.
>
> page 14, "makes use a meta token" → "makes use of a meta token"
done

> page 15, "either unconditionally (temporarily) or if an exception is raised
> (tentatively)" : I would suggest not trying to factor two sentences into one
> here, as it may cause confusion.
not done (TODO ?)

> page 15, "not only ..., but ..."
done

the quote block is superseded, we rewrote the corresponding section
> page 15, "terminating a transaction invalidates it and all the transactions
> that were valid when it was created" : this sentence should be explained.
>
> Does it imply that transactions cannot be nested?
> page 16, "Just like new transactions, capturing" : grammatically ill-formed.
> 
> page 16, "a dependency on that transaction" : a dependency of what?
> of the new snapshot?
> 
> page 16, "a snapshot captured" → "a snapshot that was captured"
> 
> page 16, "active again when restoring a snapshot from inside the transaction"
> → "active again if a snapshot that was created inside the transaction is restored"?
> 
> page 16, "invalidates it and all" → "invalidates it as well as all"
> 
> page 16, "transactions can also be active or inactive" : should this be
> "valid transactions can also be active or inactive"? (I supposed one does
> not distinguish between invalid active and invalid inactive transactions.)
> 
> Section 4.3 is difficult to follow: informal in the beginning; formal but
> complex in the middle; and the final paragraph claims that this API is
> powerful and useful in practice, but does not really explain.

> page 16, "correction" → "correctness"
done

> page 16, "When the transaction is terminated," : I suppose you mean "rolled
> back" (not committed).
done

> page 16, "Which nodes to mark is an implementation detail, as long as restore,
> commit, and rollback encounter an invalid node". It sounds as if you are saying
> that these operations *must* encounter an invalid node.
done

> page 17, "The data definitions of our reference implementation is"
> 
> page 17, "reference operations" → "operations on references"? (or name
> the operations)
done

> page 18, "Facile uses a “journaled” implementation with record elision" :
> didn't you write earlier that record elision appears nowhere in the literature
> and is a contribution of this paper?
Note: the Facile authors never explained or documented their implementation.

> page 20, "Facile has no explicit commit implementation, we simply keep" : "we"
> seems inappropriate here.
rewritten

> page 20, "and their" → "and there"
> 
> page 20, "this situation is the most common" → "this situation is the most common one"
> or "this is the most common situation"
done

> page 20, "Vector is 52× slower" → "Vector is 52× slower than Store"
> 
> page 20, "dynarray" → "vector"?
> 
> page 20, "existing programs, that perform" → remove commas or use "which"
> 
> page 20, "which requires backtracking changes" → "which requires undoing changes"?
done

> page 21, "(not just the OCaml ecosystem but also Haskell, Scala, Rust)" →
> suggest "We searched the OCaml, Haskell, Scala, and Rust ecosystems"
> 
> page 21, "as its independent library" → "as an independent library"
> 
> page 22, "requirements that makes"
> 
> page 22, "CVC5 explicitly mentions" : mentions what?
done

> In Section 6.2, I am not sure what is the point. Is your API
> deficient/unsatisfactory in some way? Are you discussing a
> potential improvement of the API, and if so, which one?
I added a mention of where Store is in the design space.

> page 23, "at point of interest" → "at points of interest"
> 
> page 23, "Moine, Charguéraud and Pottier [2022] proposes" → "propose"
> 
> page 23, "which complexifies specifications" : compared to what?
> (The answer comes in the next sentence.)
done

> page 23, "Iris support" → "Iris's support"
meh; no change

> page 23, "The value proposition" : really??
> 
> page 23, "if you know of Okasaki’s technique to amortize the reversal of a
> list to implement a persistent queue, think of a much harder version of this
> idea." This sentence sounds like it belongs in a blog post, IMHO.
> 
> page 24, "Driscoll, Sarnak, Sleator and Tarjan [1989] exposes" → "expose".
> In the same sentence, "they are not exposed" : "they" seems to refer to
> Driscoll et al.
> 
> page 24, "bounded in-degrees" → "bounded in-degree"
> 
> page 24, "Conchon and Filliâtre [2008] presents" → "present"
done

> page 24, "We posit that we can enhance" → "We are confident that we can enhance"
> 
> page 24, "a very good mental model" : suggest replacing "good" with "precise"
> or "accurate" or "clearly defined"
> 
> page 24, "The next step is the verification of record elision". Another
> natural next to step would be to specify/verify the combination on
> transactions and snapshots.
done

> page 24, "After, it would be nice" : change "After" to "Afterwards" or
> "Then" or "Thereafter"
> 
> page 24, "context-depedent"
done

> page 24, "backtrackable trail of Z3" : citation?
> 
> page 24, "the context-dependent objects of CVC5" : citation?
Precise code reference for Z3 and CVC5 are mentioned in the appendix on SAT/SMT solvers. I didn't find a way to add a mention here that felt good in the flow of the text, so I am leaving the text as-is for now.

> page 24, "one could ask to rebuild a given snapshot" : would you know how to
> implement this? A difficulty is that a single reference would then have to
> make sense in two distinct stores. (I guess this is essentially what you are
> saying in the last paragraph.)
> 
> page 24, "the most elaborate works in this direction are Chuang" : improper
> citation... Chuang is not a work.
>
> Appendix D, "typer" → "type-checker"
done

> It would be nice if (some of) Appendix D could be moved back into the main
> paper. You have done interesting bibliographic research here, and it
> strengthens the claim that Store is a useful generic abstraction.

TODO: we still have to decide which appendices to save from drowning.
