# Description

This is the output of Inferno's 'make bench', which measures the
performance of Inferno by generating a very large random term, and
type-checking it 20 times in a row. I modified the Inferno codebase to
use different implementations of Store and report the timings.

## Inference

Inferno uses a Union-Find datastructure powered by a Store to
represent types with inference variables refined by
unification. Unification of types is "atomic" with respect to failure
(any modification is reverted if unification fails in the middle), and
this uses the [tentatively] high-level wrapper of Store.

The default 'make bench' target generates a term of size 100,000, but
running the StoreVector version on this proved way to slow, so I also
made some measurements with a term of size 10,000.

StoreTransactionalRef is François' implementation
(supported non-nested transactions, which is just enough for this
use-case), and it is thus the gold standard here.

### Summary of the results

| Implementation     | Time (size = 10_000) | Time (size = 100_000) |
|:-------------------|---------------------:|----------------------:|
| `Store`            |               0.034s |                 0.32s |
| `Map`              |               0.091s |                 0.90s |
| `TransactionalRef` |               0.034s |                 0.31s |
| `Vector`           |               1.782s |               missing |

### Size 100_000

#### StoreTransactionalRef

    Generating a term of size 100_000 (seed = 23)...
    Random generation:                      0.064 seconds.
    Size of the term:                       789345 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         32365505 words.
        Type inference alone:               25180563 words.
        Elaboration alone:                  7184942 words.
    Translating...
    Translating from nominal to de Bruijn:  0.078 seconds.
    Size of the elaborated term (nominal):  1129608 words.
    Size of the elaborated term (deBruijn): 1825779 words.
    Type-checking...
    Type-checking:                          0.247 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         0.390 seconds.
        Type inference alone:               0.314 seconds.
        Elaboration alone:                  0.076 seconds.
    Done.


#### StoreMap

    Generating a term of size 100000 (seed = 23)...
    Random generation:                      0.065 seconds.
    Size of the term:                       789345 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         97589135 words.
        Type inference alone:               89500581 words.
        Elaboration alone:                  8088554 words.
    Translating...
    Translating from nominal to de Bruijn:  0.083 seconds.
    Size of the elaborated term (nominal):  1129608 words.
    Size of the elaborated term (deBruijn): 1825779 words.
    Type-checking...
    Type-checking:                          0.257 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         1.042 seconds.
        Type inference alone:               0.901 seconds.
        Elaboration alone:                  0.141 seconds.
    Done.


#### StoreBasile

    Generating a term of size 100000 (seed = 23)...
    Random generation:                      0.063 seconds.
    Size of the term:                       789345 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         34911139 words.
        Type inference alone:               27726197 words.
        Elaboration alone:                  7184942 words.
    Translating...
    Translating from nominal to de Bruijn:  0.085 seconds.
    Size of the elaborated term (nominal):  1129608 words.
    Size of the elaborated term (deBruijn): 1825779 words.
    Type-checking...
    Type-checking:                          0.267 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         0.402 seconds.
        Type inference alone:               0.322 seconds.
        Elaboration alone:                  0.080 seconds.
    Done.


### Size 10_000

#### StoreTransactionalRef

    Generating a term of size 10_000 (seed = 23)...
    Random generation:                      0.007 seconds.
    Size of the term:                       78876 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         3417713 words.
        Type inference alone:               2601726 words.
        Elaboration alone:                  815987 words.
    Translating...
    Translating from nominal to de Bruijn:  0.007 seconds.
    Size of the elaborated term (nominal):  122225 words.
    Size of the elaborated term (deBruijn): 204899 words.
    Type-checking...
    Type-checking:                          0.025 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         0.043 seconds.
        Type inference alone:               0.034 seconds.
        Elaboration alone:                  0.009 seconds.
    Done.


#### StoreVector

    Generating a term of size 10_000 (seed = 23)...
    Random generation:                      0.006 seconds.
    Size of the term:                       78876 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         189509365 words.
        Type inference alone:               188693378 words.
        Elaboration alone:                  815987 words.
    Translating...
    Translating from nominal to de Bruijn:  0.007 seconds.
    Size of the elaborated term (nominal):  122225 words.
    Size of the elaborated term (deBruijn): 204899 words.
    Type-checking...
    Type-checking:                          0.023 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         1.790 seconds.
        Type inference alone:               1.782 seconds.
        Elaboration alone:                  0.008 seconds.
    Done.


#### StoreMap

    Generating a term of size 10_000 (seed = 23)...
    Random generation:                      0.007 seconds.
    Size of the term:                       78876 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         9672248 words.
        Type inference alone:               8669055 words.
        Elaboration alone:                  1003193 words.
    Translating...
    Translating from nominal to de Bruijn:  0.007 seconds.
    Size of the elaborated term (nominal):  122225 words.
    Size of the elaborated term (deBruijn): 204899 words.
    Type-checking...
    Type-checking:                          0.025 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         0.108 seconds.
        Type inference alone:               0.091 seconds.
        Elaboration alone:                  0.017 seconds.


#### StoreBasile

    Generating a term of size 10000 (seed = 23)...
    Random generation:                      0.007 seconds.
    Size of the term:                       78876 words.
    Inference and elaboration (one run)...
    Type inference and elaboration:         3683617 words.
        Type inference alone:               2867630 words.
        Elaboration alone:                  815987 words.
    Translating...
    Translating from nominal to de Bruijn:  0.007 seconds.
    Size of the elaborated term (nominal):  122225 words.
    Size of the elaborated term (deBruijn): 204899 words.
    Type-checking...
    Type-checking:                          0.025 seconds.
    Inference and elaboration (20 runs)...
    Type inference and elaboration:         0.043 seconds.
        Type inference alone:               0.034 seconds.
        Elaboration alone:                  0.009 seconds.
    Done.


## Type checking

After elaboration, we get an explicitly-typed System F term. Inferno
also contains a type-checker for System F to verify that the
elaborated term is well-typed. This does not need to perform any
unification for inference, but we still use a union-find data
structure to check type equality: checking that two types are equal is
done by creating union-find graphs for each type, and then unifying
the two graphs. There are two reasons for this approach (instead of
a direct syntactic traversal):

1. Inferno supports equi-recursive types in the elaborated output,
   which are represented as cyclic graphs. Equality on the syntactic
   types may non-terminate.

2. Inferno supports GADTs, which are modelled in System F by explicit
   type equality hypotheses. Unification is used to decompose complex
   type equality hypotheses into equalities on type variables, and
   Union-Find is used to efficiently check type equality modulo those
   equality hypothesis.

In particular, local introduction of GADT equalities requires some
sort of backtracking support for the Union-Find data structure. Such
introductions may be nested, so François Pottier's Transactional
implementation cannot be used. Before we implemented a Store with
backtracking support, this was implemented using StoreVector.

Unlike type-checking unifications (previous section), which use fairly
short-lived transactions, backtracking is rare, only on GADT patter
matching. This corresponds to the ideal use-case for our Store
implementation.

Remark: unlike the type-inference component of Inferno, the System
F type-checker of Inferno was not written with performance in mind and
never profiled/optimized, this is a fairly naive implementation.

### Results

```
dune exec -- client/test/BenchMLRandom.exe --typechecking-runs 20 --inference-runs 1
```

| Implementation | Time (size = 100_000) | Relative |
|:---------------|----------------------:|---------:|
| `Vector`       |                 0.28s |     1.3x |
| `Map`          |                 0.88s |     4.2x |
| `Store`        |                 0.21s |     1.0x |
| `Ref`          |                 0.21s |     1.0x |

## Type checking with GADTs

I hacked around to be able to run one of the small GADT examples in
a loop, with different implementations of store. This is less reliable
than the previous result, because it is a very small program that is
being type-checked in a loop.

| Command  |      Time [s] |     Relative |
|:---------|--------------:|-------------:|
| `Store`  | 0.019 ± 0.001 |         1.00 |
| `Map`    | 0.075 ± 0.003 |  3.97 ± 0.32 |
| `Vector` | 1.295 ± 0.036 | 68.68 ± 5.46 |



