We thank our reviewers for their detailed review and feedback.

## Main questions


We identified two salient comments in the reviews.

1. Reviews B and D would prefer to see a formalization of record elision in addition to the simpler mechanics already formalized.

   We are already working on proving the correctness of record elision. We have good ideas of how to formulate the invariants, but updating the proofs is non-trivial work. We are confident that we can prove record elision correct for the final version, should this paper be accepted. Yet, the ways of mechanized proofs are inscrutable.

2. Reviewer A asks for more representative benchmarks, and reviewer C would want a benchmark exercising persistent snapshots, rather than non-persistent transactions.

   We have found a use for Store in an independent (Inferno-related) project that does use persistent snapshots. This project serves as an extra "real-life usage" point -- performance result for a complete, non-trivial application. On this project, migrating from persistent maps to our Store gave a 29% performance improvement on a representative input. We plan on adding this project as a macrobenchmark for Store in Section 5.3 of the final version.


## More comments (feel free to skip)

### Review A

> My only concern is that the comparison focuses on generic libraries.
> As you note yourself, there are only relatively few of them, and most
> people seem to roll out their own implementation for a specific use
> case. The reason is that the use cases are specialized enough and
> resource bound enough that one wants the best performance. So it is
> more convincing to compare with such specialized implementation by
> using Store in place of it, as you have done for Sudoku-solver.
> I am afraid that in many cases there will indeed be some advantage,
> either in performance or memory usage to use a hand-optimized
> implementation.

This does not match our intuition after reviewing backtracking/snapshot code in type-checkers in the wild (GHC, Scala etc.): many relative simple backtracking implementation that may in fact benefit of using a specialized library such as our own (record elision, separation of concern / maitainability).

The Sudoku solver is arguably a worst-case where the state to be backtracked has a very flat representation, easy to optimize by hand. We could support this with "custom" operations. In a SMT solver, there are arguably fewer opportunities for optimizing the state-saving logic than for Sudoku. We expect the library approach to work well, at least assuming custom operations, and in fact some of the libraries we compared against (Facile, Colibri2) are embedded inside the codebase of a solver.

> The benefit of record elision seems to depend much on the use case. For instance, in union-find, one updates a reference cell at most once, so I would expect to see no benefit.

Reads dominate writes in most union-find workflows, so indeed record elision may not play a large role. But note that it is possible for a node to be updated several times when several 'union' operations are applied. This is typically the case when using union-find for ML type inference -- many type variables will be unified together through a chain of 'union' operations, so a given variable may change representative several times.


> * p.15 sec.4.2: I am afraid I do not understand how transactions interact with snapshots.

We acknowledge that we could do a better job of explaining this tricky aspect. We plan on refomulating this section and expanding it.

> In particular I am confused about the invalidation part. Does it mean that invalidating a transaction does not invalidate transactions that were started after it?

Invalidating a transaction invalidates all snapshots and transactions that "depend" on it, that is that were created in scope of that transaction.

```ocaml
let s = create () in
let tr1 = transaction s in
(* tr1: active *)
let sn2 = capture s in (* sn2 depends on tr1 *)
rollback s tr1; (* tr1: invalid *)
(* sn2 depends on tr1, so sn2: invalid *)
restore s sn2; (* runtime error: sn2 is invalid *)
```

But: a snapshot may be created "after" a transaction in program time, yet not depend on it, if we restored an earlier snapshot to "jump out" of the transaction.

```ocaml
let s = create () in
let sn0 = capture s in
let tr1 = transaction s in
(* tr1: active *)
let sn2 = capture s in (* sn2 depends on tr1 *)
restore s0;
(* tr1: inactive *)
let sn3 = capture s in (* sn2 does not depend on tr1 *)
restore sn2;
(* tr1: active *)
rollback s tr1; (* tr1: invalid *)
restore s sn3; (* ok (no-op): sn3 is still valid *)
```

> * sec.4.3: I am still lost on the interaction between transaction and snapshot.

We do not expect typical use-cases to rely on the fine details of this interaction. But we don't have enough experience with the library yet to be confident of which aspects will and will not be useful. (For example, is the ability to "jump out" of a transaction by temporarily restoring an earlier snapshot important? We suspect so, but have not used this feature in anger yet.) We tried to work out a precise specification that allows as much as possible. We believe that it is possible to explain it clearly, and it may even be possible within the paper's page limits.


### Review B

> The macro benchmarks appear limited. It seems that a version of Inferno with "store" has been used to type check only three large terms. I wonder how representative the benchmark is.

In our defense, Inferno is a prototype type-checker that is used to research type inference techniques and their implementation, that is, on toy/core programming languages. It does not currently support an input language in which large programs have been written, so the performance tests are a bit artificial (we generated large synthetic programs).

Above we mentioned a different use-case (also type-checking related) where using Store also gives a noticeable macro-level performance benefit compared to persistent maps.


> 3. Why not define `Ref.t` as a tuple that include the `store`? This avoids passing around the `store` explicitly (and thus seems to make it impossible to create unchecked programming errors). It seems that you are doing this already on page 6, but not in the API on page 8.

Two reasons:

- We wanted to avoid the memory overhead in the base library. But it may be a reasonable higher-level layer to offer to some users.

- Our intuition is that, in common cases, being forced to explicitly pass the store state around actually helps for reasoning about the code, to keep in mind that it depends on a store (and know which part of the program access the store). In general exposing effects explicitly can be bureaucratic and hurt readability, but here we are only passing a parameter down the call stack (this is a Reader on a mutable value), so the "costs" are small.


> + page 8: Does the definition of `data` depend on support for some form of existential types in OCaml? (`'a` is not bound in the return type of `Diff`). It might be worthwhile to include a remark.

Good point! Indeed, `data` is a "generalized algebraic datatype" (GADT), `'a` is bound existentially; it is a key aspect of our library that stores are "heterogeneous", they allow tracking references of different types.

> + page 17: "or the code is correct" The fact that testing did not find a bug, does not mean it is correct. It might mean it is very likely correct.

Of course, but we are trying to emphasize that our experience using Monolith with a good model was *indistinguishable* from working with a correctness oracle. We tried various things and it always found bugs immediately in all cases of bugs that we are aware, and we have not noticed a single instance of discovering later that a bug had slipped in without being caught by the testsuite.

This feels very different from typical testing or even property-based testing, and much closer to the unreasonable effectiveness of model-checking tools in some problem domains. This seemed worth pointing out, to encourage other data structure authors to consider this approach more often.


### Review C

> The only clear limitation of the practical evaluation is the fact that, as far as I can tell, *all* the benchmarks only consider transactional cases — and thus, the core snapshot features are seemingly not tested for efficiency at all.

We mostly used our synthetic benchmarks to compare against other implementations -- the state of the art, if you wish. Those benchmarks use the semi-persistent interface mostly because we did not find another implementation of the persistent snapshots API to compare against, except for persistent maps.

We know that the snapshot implementation performs well because we measured its performance on transactional workflows, and observed that the overhead compared to the transactional implementation is relatively small -- such a micro-benchmark is mentioned in appendix B.6.2, and we also tried this on the sudoku micro-benchmark, when our persistent snapshots add 30% overhead over the baseline, compared to the 20% overhead of our transactional interface.

> While [persistent snapshots] clearly provide additional features on top of the transactional interface, this leaves the reader wondering whether they are a practically important aspect of the library. Including a benchmark or an example where they are useful would strengthen the justification for the design of the library.
> [..]
> Are you aware of any real-life problems that require the full power of persistent stores (other than the dynamic binding in LISP mentioned in the introduction)? Have you benchmarked your implementation on any such problems?

There are two different reasons to use persistent snapshots instead of the transactional API:

- Sometimes persistence is a strong requirement. A generic example is a search problem where we wish to perform a breadth-first traversal instead of a depth-first traversal. There certainly are real-life problem that benefit from BFS, or even require it.

- Sometimes it is easier to program using persistent snapshots, even though the dynamics of the program would allow using the transactional API. We encountered such a situation in our own real life (after writing this submission), where we decided to use Store in an unrelated project we are working on, which was previously using a persistent union-find using persistent maps. The program does a form of program synthesis / program search by interleaving the top-down generation of untyped terms (terms contais holes that are filled progressively) with type-inference to reject ill-typed candidates. Changing the code to use persistent snapshots is very easy (a persistent union-find is a pair of a union-find over a store and a snapshot of the store), it took a 30-lines diff to do it. Changing the code to use the transactional API would be substantially harder, requiring an invasive rewrite of the code -- the program does perform a depth-first search, but it is encapsulated under a search monad abstraction that makes it non-trivial to use ephemeral transactions.

  On this real-life program, migrating the code from persistent maps to our Store implementation (for a persistent union-find) resulted in a 29% speedup on a representative input.


> While record elision reduces the number of updates recorded for each ref cell, the diffs are still recorded per cell. Intuitively, it seems that one could push the elision further and allow for a diff-link to record multiple, essentially parallel updates to multiple cells of state. Of course, this will not change the total amount of data that needs to be saved, but maybe it could improve locality or limit the number of link traversals necessary? Have you considered a representation of this kind at all?

A previous semi-persistent-only implementation (the BacktrackingStore) uses a dynamic array to journal operations instead of our mutable linked-list representation. This has worse memory liveness properties but better locality. We expected it to perform noticeably better, but in fact we did not observe performance benefits in our benchmarks.


### Review D

> Will the Store library be open-sourced and added to OPAM?

Of course! The work is already open source and available publicly.

The main reason why we have not published on opam yet is that we are unsure of the implications in terms of strong double-blind reviewing. (Releasing on opam is probably fine, but announcing the package publicly is probably not.)

> The related work is quite thorough within the PL literature, but many of these problems are encountered in relational databases, too. Journaling, snapshots, rollbacks... all bread and butter for database research. I wonder if conducting a literature review there would reveal some scenarios that call for confluence (after all, concurrent transactions have to be merged)?

We are broadly unfamiliar with database research; the remark on confluence is interesting, thanks! Our uninformed impression is that a central aspect of database transactions is concurrency control (in addition to the idea of avoiding any partial update on failure). Concurrency influences implementation decisions and make them rather different from ours -- and sensibly more complex.

Related but different from databases, we looked at Software Transactional Memory (STM) implementations: we looked at the STM code in the GHC runtime, in ZIO (Scala), and the `kcas` library in OCaml. Our conclusion is that concurrency makes things very different, for example record elision was not applicable as-is in the STM setting: the operation log must track both the 'before' and 'after' value for modified locations, to be able to reason about consistency at commit time, so we need to do some book-keeping work on every write anyway.

