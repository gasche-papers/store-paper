# A guide to our 'store' implementation for ICFP reviewers

This is a git repository, an anonymized version of a public, open
source, non-anonymous implementation of our `Store` library.

The `main` branch contains the "clean" version of the code, the other
branches are more experimental.

## Code structure

- `src/store.mli`: public interface of the library, with user-facing documentation comments

- `src/store.ml`: implementation of the library, with implementation-specific comments.

   There are *a lot* of comments in `src/store.ml`, that we wrote
   during a pre-formalization step to ensure that we understood what
   the implementation was doing, and force us to develop or refine
   various concept that helped the formalization but also continued
   evolution of the implementation.

- `fuzzing/monolith.ml`: Monolith specification and random tests.
   Worth looking at, in our opinion, due to its unreasonable
   effectiveness at model-checking our implementation.
   
   `make random` runs the tests. (Try adding a bug in the code!)
	
	`make fuzz` runs the tests under AFL graybox fuzzing, but this is
    more painful to setup and in our experience the blackbox fuzzing
    of `make random` suffices.

- `bench/`: our benchmark apparatus, but see the `benchmark-analysis`
  branch for more details.

## Experimental branches

- `main`: the main branch in a "clean state"

- `bench-analysis`: the branch tracking our benchmarking setup. There
  is a lot more content in the `bench/` subdirectory in that branch,
  including in particular the scripts we use to produce benchmark
  results. Have a look there if you want to study our benchmarks.
  
  We are planning to clean up the history of the branch and
  merge in `main`, but were distracted by paper writing.
  
- `store-black`: an alternative implementation of Store with a
  different version graph structure: there is one node per snapshot or
  transaction, and the node tracks a list of edit actions that are
  part of this snapshot. (We wrote this as an experiment when exploring
  potential invariants for the formal proof, one formulation we are
  interested in is like this, and we tried to have an implementation that
  matches it.)

- `custom`: a first experimental branch for "custom operations",
  defining a record of functions that capture the interface between
  the store and a custom "field" or "location" in memory, with an
  implementation of stored hash tables on top of it.
  
- `monoid`: a second experimental branch for "custom operations", that
  provides a more general (but also costlier) interface for custom
  operations. In this more general version, record elision is weakened
  in the sense that write to a previously-recorded location needs not
  create a new record, but it may need to update the previous record
  with further undo information -- antioperations can be composed
  and form a monoid structure.
