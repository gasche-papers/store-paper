if [ ! -f build.sh ]
then
    echo "Error: this script must run from the 'icfp24-submission' directory."
    exit 2
fi

if [ ! -d /tmp/store.git ]
then
    echo "Error: /tmp/store.git is missing, you should run the scripts in ../censoring-git-repo first."
    exit 2
fi

set -x

rm store-proofs.zip
rm store-implem.zip

# we use a local 'tmp' subdirectory and destroy its content
rm -fR ./tmp/
mkdir -p ./tmp

cp -r /tmp/store.git ./tmp/store-implem
cp impl.README.ICFP.md ./tmp/store-implem/README.ICFP.md

grep -i -E "(Alexandre|Basile|Cl[eé]ment|Gabriel|Guillaume)" -R store-proofs -R ./tmp/store-implem

zip --quiet -r store-proofs store-proofs

(cd ./tmp/ ; zip --quiet -r store-implem store-implem)
mv ./tmp/store-implem.zip store-implem.zip
rm -fR ./tmp

