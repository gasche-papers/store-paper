> ICFP 2024 Paper #48 Reviews and Comments
> ===========================================================================
> Paper #48 Snapshottable stores
>
>
> Review #48A
> ===========================================================================
>
> Overall merit
> -------------
> B. OK paper, but I will not champion it
>
> Reviewer Expertise
> ------------------
> Y. I am knowledgeable in this area, but not an expert
>
> Reviewer Confidence
> -------------------
> 3. Good: I am reasonably sure of my assessment
>
> Paper summary
> -------------
> This paper introduces a library Store moving efficiently between
> multiple states of a mutable store. It provides more than just
> backtracking, as one can retain any state and go back it after
> arbitrary state switches (i.e., one is not restricted to linearly
> ordered states).
> This library is imperative, efficient, and its core algorithm was
> proved using Iris.
> Both micro- and macro-benchmark demonstrate its efficiency.
>
> Assessment of the paper
> -----------------------
> Strengths:
> * Having a proved library for a non-trivial algorithm is a boon. The
>   benchmarks show that the library is highly efficient.
> * Explanations about the snapshot tree are very clear.
>
> Weakness:
> * The benchmarks essentially only compare to other library-based
>   approaches, where Store uniformly matches the best.
>   The only comparison to a hand optimized approach the Sudoku-solver,
>   for which the hand optimized approach is 17% faster than Store.
> * Explanations about transactions and their interactions with snapshot
>   are much less clear.
>
> Conclusion:
> This is a nice library with many fine applications.
> However, I am not sure this is the ultimate solution.
> Namely, people will probably still work out their own solution to grab
> the few extra percents of performance.
>
> Comments for authors
> --------------------
> Overall this paper is an enjoyable read.
> This looks like a nice and versatile library working with multiple
> states.
>
> My only concern is that the comparison focuses on generic libraries.
> As you note yourself, there are only relatively few of them, and most
> people seem to roll out their own implementation for a specific use
> case. The reason is that the use cases are specialized enough and
> resource bound enough that one wants the best performance. So it is
> more convincing to compare with such specialized implementation by
> using Store in place of it, as you have done for Sudoku-solver.
> I am afraid that in many cases there will indeed be some advantage,
> either in performance or memory usage to use a hand-optimized
> implementation.

This does not match our intuition after reviewig backtracking/snapshot code in type-checkers in the wild (GHC, Scala etc.): many relative simple backtracking implementation that may in fact benefit of using a specialized library such as our own (record elision, separation of concern / maitainability).

The Sudoku solver is arguably a worst-case where the state to be backtracked has a very flat representation, easy to optimize by hand We could support this with "custom" operations. In a SMT solver, there are arguably less opportunities for optimizing the state-saving logic than for Sudoku. We expect the library approach to work well, at least assuming custom operations, and in fact some of the libraries we compared against (Facile, Colibri2) are embedded inside the codebase of a solver.

> This said, at the very least this looks like a great tool for
> prototyping, and amply sufficient if you don't need the last 20% or
> so.
>
> Other comments:

> * p.8 l.362: What is the meaning of "Mem"
done

> * p.11 l.512:
>   The benefit of record elision seems to depend much on the use case.
>   For instance, in union-find, one updates a reference cell at most
>   once, so I would expect to see no benefit.

We agree that reads dominate writes in most union-find workflows, but note that it is possible for a node to be updated several times when several 'union' operations are applied. This is typically the case when using union-find for ML type inference -- many different type variables will be unified together through a chain of 'union' operations, so they will change representatives several times.

> * p.14 l.671: "Notice that this is not a map".
>   What is not a map ? I did not understand.
done

> * l.678: What is a camera ?
TODO: if there is a clearer way to reformulate this, Iris experts would know better than I do. Maybe it is enough to just say that this is a Iris technical concept?
AM: I translated to "resource algebra". Is this enough? It is still
slightly correct, and less scary than the obscure "camera".

> * p.15 sec.4.2: I am afraid I do not understand how transactions
>   interact with snapshots.

We hear that we could do a better job of explaining this tricky aspect, and will try to refomulate this section and expand it.

TODO reformulate this section

> In particular I am confused about the invalidation part. Does it mean that invalidating a transaction does not invalidate transactions that were started after it?

Invalidating a transaction invalidates all snapshots and transactions that "depend" on it, that is that were created in scope of that transaction.

```ocaml
let s = create () in
let tr1 = transaction s in
(* tr1: active *)
let sn2 = capture s in (* sn2 depends on tr1 *)
rollback s tr1; (* tr1: invalid *)
(* sn2 depends on tr1, so sn2: invalid *)
restore s sn2; (* runtime error: sn2 is invalid *)
```

But: a snapshot may be created "after" a transaction in program time, yet not depend on it, if we restored an earlier snapshot to "jump out" of the transaction.

```ocaml
let s = create () in
let sn0 = capture s in
let tr1 = transaction s in
(* tr1: active *)
let sn2 = capture s in (* sn2 depends on tr1 *)
restore s0;
(* tr1: inactive *)
let sn3 = capture s in (* sn2 does not depend on tr1 *)
restore sn2;
(* tr1: active *)
rollback s tr1; (* tr1: invalid *)
restore s sn3; (* ok (no-op): sn3 is still valid *)
```

> * sec.4.3: I am still lost on the interaction between transaction and
>   snapshot.

We do not expect typical use-cases to rely on the fine details of this interaction. But we don't have enough experience with the library yet to be confident of which aspects will and will not be useful. (For example, is the ability to "jump out" of a transaction by temporarily restoring an earlier snapshot important? We suspect so, but have not used this in anger yet.) So we tried to work out a precise specification that allows as much as possible. We believe that it is possible to explain it clearly, and it may even be possible within the paper's page limits.

> Review #48B
> ===========================================================================
>
> Overall merit
> -------------
> B. OK paper, but I will not champion it
>
> Reviewer Expertise
> ------------------
> Y. I am knowledgeable in this area, but not an expert
>
> Reviewer Confidence
> -------------------
> 3. Good: I am reasonably sure of my assessment
>
> Paper summary
> -------------
> This paper presents a "snapshottable store": an implementation of ML-style references (allocate, load, store) with the option to make a snapshot of a bag of references and roll back to prior versions. While there exists specialized data structures with snapshots, the one introduced in the paper is claimed to be sufficiently general that it can be used to implement other snapshottable data structures.
>
> The contributions of the paper are:
>
> + A description of the implementation: a simple "core" version (based on Baker's version trees), an optimized version (with a new technique called "record elision"), and an extended version with transactions.
> + A mechanized proof of correctness of the "core" version in the Iris framework in Coq.
> + An implementation of the extended version in OCaml, which has been tested on micro and macro benchmarks.
>
> Assessment of the paper
> -----------------------
> I found the paper interesting and pleasant to read. The paper clearly motivates the problem, the data structure, the way it is used, and the reasons why it is correct.
>
> I have some smaller concerns:
>
> + The "core" version is verified, while only a proof sketch for optimized version with record elision is claimed to exist. Since the "core" version is essentially Baker's (the authors state that on p10) and record elision is the key optimization introduced in this paper, I would expect the full proof to be present.

We are interested in working on it and suspect that we could get a proof of correctness of record elision by the camera-ready deadline. ("Suspect" instead of "claim" as the ways of Coq proofs are inscrutable.)

> + The macro benchmarks appear limited. It seems that a version of Inferno with "store" has been used to type check only three large terms. I wonder how representative the benchmark is.

Inferno is a prototype type-checker that is used to research type inference techniques and their implementation -- on toy/core programming languages. It does not currently support an input language in which large programs have been written, so the performance tests are a bit artificial (we generate large synthetic programs).

Since the paper was written we used Inferno-with-store in another application (generating well-typed lambda-terms) that uses persistent snapshots -- not just semi-persistent transactions. The use of our Store implementation instead of persistent maps gives a 29% performance improvement to our final application for one representative input.

> Questions for authors’ response
> -------------------------------
> 1. Will the verification of the optimized version be finished for the final version of the paper?
> 2. Could you comment on my concern regarding macro benchmarks?
> 3. Why not define `Ref.t` as a tuple that include the `store`? This avoids passing around the `store` explicitly (and thus seems to make it impossible to create unchecked programming errors). It seems that you are doing this already on page 6, but not in the API on page 8.

Two reasons:

- We wanted to avoid the memory cost in the base library, but it may
  be a reasonable abstraction to offer to some users.

- Our intuition is that, in common cases, being forced to explicitly
  pass the store state around actually helps for reasoning about the
  code, to keep in mind that it depends on a store (and know which
  part of the program access the store). In general exposing effects
  explicitly can be bureaucratic and hurt readability, but here we are
  only passing a parameter down the call stack (this is a Reader on
  a mutable value), so the "costs" are small.

> Comments for authors
> --------------------
> + page 6/7: The tree and explanation for the state after `Store.snapshot s` is missing in Fig 1.

done

> + page 7: There is a spurious argument `s` in `Store.set s r 2` (see also question above).

... in fact we had forgotten `s` all over the place

> + page 8: Does the definition of `data` depend on support for some form of existential types in OCaml? (`'a` is not bound in the return type of `Diff`). It might be worthwhile to include a remark.

Good point! Indeed `data` is a "generalized algebraic datatype" (GADT), `'a` is bound existentially; it is a key aspect of our library that stores are "heterogeneous", they allow to track references of different types.

> + page 8: Inconsistent spacing in `Diff(r, v,B)`.
done

> + page 13: I am not sure Vindum/Birkedal is the right citation for persistent assertions in Iris; they have been in Iris since the beginning (they were still called "pure" in Jung et al. 2015; the terminology persistent has been used since Jung et al. 2016). Vindum/Birkedal proposed the "persistent points-to" predicate, but that is not used here.

done, it was a typo.

> + page 14: "effiectievly"
done

> + page 14: "ghost cell" and "ghost location" different name for same thing?

done. I (alexandre) unified to "ghost location", the one appearing
most in the ground-up paper.

> + page 14: Use `$\mathit{Value}$` not `$Value$`, the spacing in the latter is strange.
done

> + page 15: overfull hbox
ignored for now

> + page 17: "or the code is correct" The fact that testing did not find a bug, does not mean it is correct. It might mean it is very likely correct.

Of course, but we are trying to emphasize that our experience using Monolith with a good model was *indistinguishable* from working with a correctness oracle. We tried various things and it always found bugs immediately in all cases of bugs that we are aware, and we have not noticed a single instance of discovering later that a bug had slipped in without being caught by the testsuite.

This feels very different from typical testing or even property-based testing, and much closer to the unreasonable effectiveness of model-checking tools in some problem domains.

> + page 19: Use consistent labels in Fig 5: "set 1" and "set 16"
done

> + page 20: "and their Vector is 6×" Grammar "there"?
> + page 23: "Puente [2017], [Stokke 2018]," Inconsistent use of \citet and \cite.
done

> + page 23: "Moine, Charguéraud and Pottier [2022] proposes the only formal verification" Why isn't this paragraph in 6.5 on formal verification?
done

> + page 23: Verification of transactions, if you consider concurrency there is surely more work, e.g., Chang et al. OSDI'23 in Iris.

Not sure if that is really relevant as the implementation is very different from ours.
Do we want to please the reviewer with one extra citation?

> + page 24: "Demaine, Langerman and Price [2008] presents" should be plural "present" (same error occurs more often)
It depends if we are thinking of "the article of ..." or "the authors of ...". No strong opinion.

> + page 25: "context-depedent"
done

>
>
> Review #48C
> ===========================================================================
>
> Overall merit
> -------------
> A. Good paper, I will champion it
>
> Reviewer Expertise
> ------------------
> Y. I am knowledgeable in this area, but not an expert
>
> Reviewer Confidence
> -------------------
> 3. Good: I am reasonably sure of my assessment
>
> Paper summary
> -------------
> The paper treats a very natural problem in functional programming with imperative features, namely, the impersistence of mutable state. The authors propose an OCaml library that extends the built-in state cells with a notion of persistent snapshots, and the ability of the updates to the state to be rolled back to any of the snapshots. The resulting state is still ephemeral, since there is only ever one "current" state — but this allows the library to be efficiently implemented. The crucial observation is that taking snapshots efficiently allows for a less defensive treatment of history through forgetting updates that can never be recovered.
>
> On top of this infrastructure, the library builds a specialised infrastructure for the most commonly encountered patterns of usage. The authors build a model of (a simplified version of) their library and prove it correct using separation logic, and provide a number of benchmarks to argue for the implementation's efficiency.
>
> Assessment of the paper
> -----------------------
> While the authors start their implementation from a classic technique, and the key observation seems rather straightforward, its impact appears to be far-reaching. The design of the library and the representation of its data structures prove clear and easy to understand, and the higher-level features built on top of it support the most common usage patterns. The verification effort provides a good model and enhances the understanding of the fundamental concept, and the benchmarks provide empirical data to justify that the implementation performs well in the common usage patterns (although, unsurprisingly, it does not always remain on par with custom-tailored stores). The presentation is lucid and the paper was a pleasure to read.
>
> The only clear limitation of the practical evaluation is the fact that, as far as I can tell, *all* the benchmarks only consider transactional cases — and thus, the core snapshot features are seemingly not tested for efficiency at all. While they clearly provide additional features on top of the transactional interface, this leaves the reader wondering whether they are a practically important aspect of the library. Including a benchmark or an example where they are useful would strengthen the justification for the design of the library.

We mostly used our synthetic benchmarks to compare against other implementations -- the state of the art, if you wish. Those benchmarks use the semi-persistent interface mostly because we did not find another implementation of the persistent snapshots API to compare against, except for persistent maps.

We know that the snapshot implementation performs well because we measured its performance on transactional workflows, and observed that the overhead compared to the transactional implementation is relatively small -- such a micro-benchmark is mentioned in appendix B.6.2, and we also tried this on the sudoku micro-benchmark, when our persistent snapshots add 30% overhead over the baseline, compared to the 20% overhead of our transactional interface.

> Even without the practical evaluation of the full power of persistent stores, I think this paper represents a strong contribution, and I am in favour of accepting it.
>
> Questions for authors’ response
> -------------------------------
> Are you aware of any real-life problems that require the full power of persistent stores (other than the dynamic binding in LISP mentioned in the introduction)? Have you benchmarked your implementation on any such problems?

There are two different reasons to use persistent snapshots instead of the transactional API:

- Sometimes persistence is a strong requirement. A generic example is a search problem where we wish to perform a breadth-first traversal instead of a depth-first traversal. There certainly are real-life problem that benefit from BFS, or even require it.

- Sometimes it is easier to program using persistent snapshots, even though the dynamics of the program would allow using the transactional API. We encountered such a situation in our own real life (after writing this submission), where we decided to use Store in an unrelated project we are working on, which was previously using a persistent union-find using persistent maps. Changing the code to use persistent snapshots is very easy (a persistent union-find is a pair of a union-find over a store and a snapshot of the store), it took a 30-lines diff to do it. Changing the code to use the transactional API would be substantially harder, requiring an invasive rewrite of the code -- the program does perform a depth-first search, but it is encapsulated under a search monad abstraction that makes it non-trivial to use ephemeral transactions.

  On this real-life program, migrating the code from persistent maps to our Store implementation (for a persistent union-find) resulted in a 29% speedup on a representative input.

> Comments for authors
> --------------------
> While record elision reduces the number of updates recorded for each ref cell, the diffs are still recorded per cell. Intuitively, it seems that one could push the elision further and allow for a diff-link to record multiple, essentially parallel updates to multiple cells of state. Of course, this will not change the total amount of data that needs to be saved, but maybe it could improve locality or limit the number of link traversals necessary? Have you considered a representation of this kind at all?

A previous semi-persistent-only implementation (the BacktrackingStore) used a dynamic array to journal operations instead of our mutable linked-list representation. This has worse memory liveness properties, and we did not observe performance benefits in our benchmarks. A dynamic array could perform better for *extremely* large logs (where locality benefits show up stronger), which we don't think are realistic for most applications, or maybe in other programming languages that do not use a write barrier.

> Review #48D
> ===========================================================================
>
> Overall merit
> -------------
> A. Good paper, I will champion it
>
> Reviewer Expertise
> ------------------
> X. I am an expert in this area
>
> Reviewer Confidence
> -------------------
> 3. Good: I am reasonably sure of my assessment
>
> Paper summary
> -------------
> This paper introduces an OCaml library, Store, that efficiently abstracts a number of well known approaches for creating snapshots of program state. Store supports creating both persistent and semi-persistent (no branching) snapshots and restoring them through an explicit API. The implementation is subtle and a Coq proof of correctness is provided for the core technique. A performance study is conducted that shows Store meets its performance goals.
>
> Assessment of the paper
> -----------------------
> This paper presents its contributions very well, the writing is clear and engaging, and the result appears to be an efficient and reusable library to replace common ad-hoc patterns in compiler construction, theorem proving, and beyond.
>
> I'm not sure the present formalization does much for readers, given the absence of record elision. I was convinced of the correctness of the method from the paper alone, so I would want a mechanized proof to convince me the _implementation_ is correct, too. Clearly, formalization was helpful in producing the implementation in the first place.
>
> I really appreciated the performance study; it built a lot of confidence in the technique for me. In particular, the discussion of liveness was very compelling.
>
> I think this paper is clearly above the bar. I am reminded of the classic precedence-climbing method for parsing expressions: both techniques are easy to implement (once you _get_ them), highly reusable, and efficient enough to be used outside prototyping. Are there faster persistent data structures and expression parsers? Sure, but nothing with quite the same effort-to-reward ratio. I look forward to using this in my own work.
>
> Questions for authors’ response
> -------------------------------
> Will the Store library be open-sourced and added to OPAM?

Of course! The work is already open source and available publicly.

The main reason why we have not published on opam yet is that we are unsure of the implications in terms of strong double-blind reviewing. (Releasing on opam is probably fine, but announcing the package publicly is probably not.)

> The related work is quite thorough within the PL literature, but many of these problems are encountered in relational databases, too. Journaling, snapshots, rollbacks... all bread and butter for database research. I wonder if conducting a literature review there would reveal some scenarios that call for confluence (after all, concurrent transactions have to be merged)?

We are broadly unfamiliar with database research. Our uninformed impression is that a central aspect of database transactions is concurrency control (in addition to the idea of avoiding any partial update on failure). Concurrency influences implementation decisions and make them rather different from ours -- and sensibly more complex.

Related but different from databases, we looked at Software Transactional Memory (STM) implementations: we looked at the STM code in the GHC runtime, in ZIO (Scala), and the `kcas` library in OCaml). Our conclusion is that concurrency makes things very different, for example record elision was not applicable as-is in the STM setting: the operation log must track both the 'before' and 'after' value for modified locations, to be able to reason about consistency at commit time, so we need to do some book-keeping work on every write anyway.

> Comments for authors
> --------------------
> I'm not surprised that a sudoku solver has faster backtracking and I don't consider that fact a threat to this paper. I'm sure the same is true for chess engines, too.
>
> Back of the envelope: a 9x9 sudoku board consists of 81 cells that have up to 10 distinct states. So 4 bits suffices per cell, for 41 bytes total. Modern CPUs have 64-byte cache lines. Both NEON and AVX-512 have single instructions that can save/restore such a snapshot. It's easy to see why it doesn't make sense to track per-cell differences when the structure is so small.
>
> A 25x25 board is worse: 418 bytes for a packed 3x5-bit-in-16-bit representation, but you can still fit many of these in cache, within a single page.
>
> ---


> Fig 6(c) has "transactions" misspelled (missing the "n").
done
